![Build Status](https://gitlab.com/ucl-pba-wu/22e-wu-wep-data/badges/main/pipeline.svg)

# 22e-wu-wep-data

Side til undervisningsmateriale vedr. webudvikling webprogrammering/database faget.

Link til website: [https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/](https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/)

# Efter projektet er klonet

1. Lav et virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) windows: `py -m venv env`
2. Aktiver virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment) windows: `.\env\Scripts\activate`
3. Installer pip dependencies `pip install -r requirements.txt`
4. Kør siden lokalt `mkdocs serve` fra **pages** mappen

## Dokumentation

- MKDocs [https://www.mkdocs.org/](https://www.mkdocs.org/)
- Theme [https://github.com/squidfunk/mkdocs-material](https://github.com/squidfunk/mkdocs-material)
- More on Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
- Git revision plugin [https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/](https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/)
- linkchecker [https://github.com/scivision/linkchecker-markdown](https://github.com/scivision/linkchecker-markdown)
- PDF builder [https://github.com/brospars/mkdocs-page-pdf](https://github.com/brospars/mkdocs-page-pdf)
