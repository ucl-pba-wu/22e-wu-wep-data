---
hide:
  - footer
---

# Opgave 43 - Node.js

## Information

Javascript er et fortolket sprog, som kræver en fortolker, for at gøre den javascript kode du skriver forståelig for din computers CPU (Central Processing Unit).
Et andet populært fortolket sprog er Python som kræver at du installerer en Python fortolker før Python koden kan forstås af din CPU.

Der findes også sprog som skal kompileres, det vil sige oversættes til en anden fil (binær) som så er den fil CPU'en forstår. Eksempler her er C og C++

Det er ikke helt korrekt at javascript er fortolket, det bliver egentlig kompileret med en teknik der kaldes Just In Time compilation (JIT) hvilket betyder at koden kompileres, mens den afvikles, modsat kompilering før koden afvikles som i de kompilerede sprog. 
Du kan læse mere om emnet her [https://en.wikipedia.org/wiki/Just-in-time_compilation](https://en.wikipedia.org/wiki/Just-in-time_compilation)

C# bruger også JIT kompilering og processen er beskrevet her [https://www.geeksforgeeks.org/how-c-sharp-code-gets-compiled-and-executed/](https://www.geeksforgeeks.org/how-c-sharp-code-gets-compiled-and-executed/)

Webbrowsere har en indbygget javascript fortolker/JIT kompiler og det er derfor de kan eksekvere javascript. Hvis du ønsker at eksekvere javascript uden en browser kræver det en javascript fortolker. Det er her Node.js kommer ind i biledet.

Node.js er groft sagt chrome browseren javascript motor uden øvrig funktionalitet som chrome tilbyder. Motoren hedder V8.
Du kan læse mere om Node.js brug af V8 her [https://nodejs.dev/en/learn/the-v8-javascript-engine/](https://nodejs.dev/en/learn/the-v8-javascript-engine/) 

Opsummeret betyder det at du, for at kunne eksekvere javascript uden din browser er nød til at installere en javascript motor som Node.js

*Fun Fact* En anden javascript motor er **Deno** som er bygget af den samme udvikler, Ryan Dahl, som startede Node.js projektet.  
Deno er Node stavet baglæns og Ryan Dahl annoncerede Deno på JSConf i 2018, i en populær talk der hedder [10 Things I Regret About Node.js](https://youtu.be/M3BM9TB-8yA)  
Du kan læse mere om Deno her [https://en.wikipedia.org/wiki/Deno_(software)](https://en.wikipedia.org/wiki/Deno_(software))

## Node Package Manager

Node har et stort community (på forskellige platforme) og også et stort 3. Parts bibliotek der hedder Node Package Manager (NPM).
Node Packages kaldes også moduler og er biblioteker du kan installere i en node.js applikation for at implemetere funktionalitet der ikke er indbygget i node.  

Pr. September 2022 er der udgivet over 2.1 million NPM moduler som du kan bruger i din node applikation.  
Du kan læse om NPM her [https://nodejs.dev/en/learn/an-introduction-to-the-npm-package-manager/](https://nodejs.dev/en/learn/an-introduction-to-the-npm-package-manager/)

Du kan søge efter NPM pakker her [https://www.npmjs.com/](https://www.npmjs.com/)

Når du laver et Node projekt oprettes en fil der hedder `package.json` hvor de NPM pakker du har installeret er angivet. Det betyder at hvis du skal bruge et node projekt på en anden computer hvor du ikke har installeret de pågældende NPM pakker, kan du installere dem let ved at skrive `npm install`. Kommandoen kigger på listen i package.json og installere de nødvendige pakker som projektet er afhængig af.

Node pakker installeres i mappen node_modules som en undermappe til dit node projekt. Fordi du kan hente projektets pakker med `npm install` er det god praksis at tilføje mappen til din `.gitignore` fil så den ikke inkluderes i dit git repository.


- overblik
- npm/package managers
- versionering
- npm scripts
- require til at inkludere moduler i applikationer
- Exports

## Instruktioner

1. Installer seneste LTS (long term support) version af Node.js, npm installeres automatisk sammen med node.js [https://nodejs.org/en/](https://nodejs.org/en/)
2. Verificér at node er installeret med kommandoen `node --version` hvilket returnerer den version af node som du har installeret
3. Verificér at npm er installeret med kommandoen `npm --version` hvilket returnerer den version af node som du har installeret 
2. Lav en mappe der hedder `node_hello_world` og åbn mappen i Visual Studio Code 
3. Åbn en terminal i mappen (jeg bruger den indbyggede i Visual Studio Code)
4. Lav en nyt node.js projekt ved at skrive kommandoen `npm init`, du bliver præsenteret for en masse valg, det eneste du skal udfylde er `package name`, alt andet trykker du bare enter
  ```
  package name: (ucl-pba-wu) node_hello_world
  version: (1.0.0)
  description:
  entry point: (index.js)
  test command:
  git repository:
  keywords:
  author:
  license: (ISC)
  ```
5. I mappen bør du nu have en package.json fil hvor indholdet er
  ```js
  {
    "name": "node_hello_world",
    "version": "1.0.0",
    "description": "",
    "main": "index.js",
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1"
    },
    "author": "",
    "license": "ISC"
  }
  ```
6. Lav en fil der hedder index.js og skriv følgende javascript `console.error('Hello node.js')` (husk at gemme filen)
7. Kør filen med kommandoen `node index.js`  
Hvis alt er ok bør der nu i terminalen stå **Hello node.js** og du har hermed bekræftet at du kan eksekvere javascript filer med V8 javascript motoren i node.js

## Links

- Javascript engine [https://en.wikipedia.org/wiki/JavaScript_engine](https://en.wikipedia.org/wiki/JavaScript_engine)