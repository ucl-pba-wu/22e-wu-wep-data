---
hide:
  - footer
---

# Opgave 26 - SQL stored procedures

## Information

Stored procedures i SQL er en samling af forespørgsler.
Stored procedures kan kaldes af applikationer, SQL triggers [https://www.geeksforgeeks.org/sql-triggers/](https://www.geeksforgeeks.org/sql-triggers/) eller andre stored procedures.

Stored procedures kan, ligesom funktioner, have input parametre og de kan også have output parametre.  
Stored procedures kan have "side effects", det vil sige at en stored procedure modificere og ændre data og tabeller i databasen.

### View vs. Stored procedure

Hvad er forskellene på et view og en stored procedure ?

|View|Stored procedure|
| --- | --- |
|Kan ikke bruge parametre (er statisk)| Kan bruge parametre (er dynamisk)|
|Kan bruges i en større forespørgsel med andre kommandoer| Kan ikke bruges i en større forespørgsel|
|Kun en `SELECT` kommando pr. view| Kan indeholde flere kommandoer og kontrol strukturer som loops, if/else osv.|
|Kan ikke ændre i databasen `read only`| Kan ændre i databasen|


### Stored procedures syntaks

En simpel stored procedure oprettes i en database ved at bruge `CREATE PROCEDURE` kommandoen.

```SQL
CREATE PROCEDURE nameOfStoredProc
AS
BEGIN
	-- statements go here
END
``` 

Hvis du vil ændre en eksisterende stored procedure skal du bruge `ALTER PROCEDURE` kommandoen

```SQL
ALTER PROCEDURE nameOfStoredProc
AS
BEGIN
	-- statements go here
END
```

En stored procedure kan slettes ved at bruge `DROP PROCEDURE` kommandoen

```SQL
DROP PROCEDURE nameOfStoredProc
```

Når en stored procedure skal eksekveres bruges `EXECUTE` eller `EXEC` kommandoen.  
Det kan udelades, men jeg foretrækker at skrive det så intentionen med min kommando er klar og tydelig for andre.

```SQL
EXEC nameOfStoredProc
-- Eller
EXECUTE nameOfStoredProc
```

For at se indholdet af en oprettet stored procedure kan du skrive

```SQL
/*sp_helptext <view_navn>*/
sp_helptext spGetCountryNameByCode
```

![images/sp_what_is_stored.png](images/sp_what_is_stored.png)


### Input parametre

Som nævnt kan en stored procedure også have input parametre, det kender du allerede fra SQL funktioner. 
Her er det til forskel fra de indbyggede funktioner dig der definerer input parametrene.

Et eksempel på en stored procedure med input parametret `Code`

```SQL
CREATE PROCEDURE spGetCountryNameByCode
@Code CHAR(3)
AS
BEGIN
   SELECT Name FROM Country WHERE Code = @Code
END
```

Læg mærke til at parametret angives med `@` symbolet samt at parametrets datatype angives `CHAR(3)`

Du kan se hvilke stored procedures der er oprettet i databasen ved at kigge i databasens mapper

![images/sp_foldes.png](images/sp_folder.png)

Det er muligt at have så mange parametre som du har lyst til i din storedprocedure, her er et eksempel med 2 parametre.

```SQL
CREATE PROCEDURE spGetCitiesByCountryAndMinPop
@CountryCode CHAR(20),
@Pop int
AS
BEGIN
   SELECT *
   FROM City
   INNER JOIN Country
       ON City.CountryCode = Country.Code
   WHERE Country.Code = @CountryCode AND city.Population >= @Pop
END
```

Når du bruger proceduren `spGetCitiesByCountryAndMinPop` er det vigtigt at du angiver parametrene i den rigtige rækkefølge og korrekt datatype, ellers får du fejl

**Rigtig parameter rækkefølge**  
```SQL
EXEC spGetCitiesByCountryAndMinPop 'DNK', 100000
```

**Forkert parameter rækkefølge**  
```SQL
EXEC spGetCitiesByCountryAndMinPop 100000, 'DNK'
```

**Fejl**

```SQL
Msg 8114, Level 16, State 5, Procedure spGetCitiesByCountryAndMinPop, Line 0
Error converting data type varchar to int.
```

### Eksekvering af stored procedure

En stored procedure anvendes ved at bruge `EXECUTE` kommandoen og derefter navnet på proceduren.  
Hvis din stored procedure bruger parametre skal de angives efter procedurens navn.

```SQL
EXECUTE spGetCountryNameByCode 'AUT'
```

Hvis du glemmer parametret får du en fejl

```SQL
Msg 201, Level 16, State 4, Procedure spGetCountryNameByCode, Line 0
Procedure or function 'spGetCountryNameByCode' expects parameter '@Code', which was not supplied.
```

### Stored procedure fordele og ulemper

Der er både fordele og ulemper ved at bruge store procedures. Her er et par af de væsentlige.  

|Fordel|Ulempe|
| --- | --- |
|Kan genbruges på tværs af flere applikationer| Det kan være besværligt at udvikle stored procedures der har meget komplekse forespørgsler|
|Sikkerhed, database administratoren kan give visse brugere adgang til en stored procedure fremfor alle tabeller i databasen|Det kan være besværligt at fejlfinde komplekse stored procedures|
|Forebygger SQL injection angreb|Det er svært at holde styr på historik og versionering af procedurene|
|Reducerer netværkstrafik|Business logik findes både i database og applikation (coupling problem)|
|Øger performance for de applikationer der bruger procedurerne||
|Indkapsling af business logik så applikationer ikke får andet end de har brug for fra databasen||

## Instruktioner
  
Hvis du får oprettet en forkert stored procedure mens su arbejder, så brug den relevante syntaks til at ændre eller slette proceduren.

1. Lav denne forespørgsel om til en stored procedure i world databasen
   ```SQL
   SELECT CountryCode, COUNT(*) AS CitiesPerCountry
   FROM City
   GROUP BY CountryCode;
   ```
2. Test proceduren så du ved at den virker
3. Lav denne forespørgsel om til en stored procedure i world databasen, proceduren skal være dynamisk så indbygger tallet er et parameter og proceduren kan eksekveres som `EXEC spSumPerRegion 100000`
   ```SQL
   SELECT region, SUM(population) AS SumPerRegion
   FROM country
   GROUP BY region
   HAVING SUM(population) > 300000000
   ORDER BY SumPerRegion DESC;
   ```
4. Test proceduren så du ved at den virker med angivelse af forskelligt antal indbyggere
5. Lav denne forespørgsel om til en stored procedure i northwind databasen, Employees.FirstName og Employees.LastName skal være dynamiske parametre
   ```SQL
   SELECT OrderID, Customers.CustomerID, CompanyName, Contactname, CONCAT(FirstName, ' ', LastName) as 'Handled By'
   FROM Orders
   INNER JOIN Customers
   ON orders.CustomerID = customers.CustomerID
   INNER JOIN Employees
   ON Orders.EmployeeID = Employees.EmployeeID
   WHERE Employees.FirstName = 'Andrew' AND Employees.LastName = 'Fuller'
   ORDER BY OrderID;
   ```
6. Test proceduren så du ved at den virker med denne kommando `EXEC spHandledByEmployee Janet, Leverling`
7. Find en af dine tidligere forespørgsler fra en valgfri database og omskriv den til en stored procedure. Der skal være minimum et dynamisk parameter, husk at teste proceduren :-)

## Links

- Microsoft stored procedures dokumentation [https://learn.microsoft.com/en-us/sql/t-sql/statements/create-procedure-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/statements/create-procedure-transact-sql)