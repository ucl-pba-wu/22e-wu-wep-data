---
hide:
  - footer
---

# Opgave 22 - Gruppering af data

## Information

I en SQL forespørgsel er det muligt at gruppere resultater, det er også muligt at specificere grupperinger nærmere ved at bruge `HAVING`.

Denne opgave forklarer gruppering og specificering ved hjælp af den officielle Microsoft dokumentation samt eksempler på forespørgsler og hvilke problemer der typisk kan optræde ved brug af gruppering.

### GROUP BY

I T-SQL kan data grupperes som en del af en forespørgsel, udsagnet til at gøre det hedder `GROUP BY`.
Dokumentationen for `GROUP BY` er her:  
[https://learn.microsoft.com/en-us/sql/t-sql/queries/select-group-by-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/queries/select-group-by-transact-sql)

Som nævnt i **Opgave 22 - Funktioner i SQL** bruges aggregate funktionerne ofte sammen med group by til at gruppere resultater og udføre en aggregate funktion på gruppen.

I dette eksempel ønskes en forespørgsel, i en tænkt database, der viser summen af lønudgifter pr. afdeling
![images/aggregate_group_by.png](images/aggregate_group_by.png)

Forespørgslen kan laves ved hjælp af `GROUP BY` og aggregate funktionen `SUM`:

```SQL
SELECT department_id, SUM(salary)
FROM employees
GROUP BY department_id;
```

Her er 4 eksempler på `GROUP BY` sammen med forskellige `SUM` og `COUNT` aggregate funktionerne

```SQL
USE world;

SELECT continent, COUNT(*) AS CountriesPerContinent
FROM country
GROUP BY continent;

SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
ORDER BY SumPerRegion DESC;

SELECT countrycode, SUM(percentage) AS SumOfficialLang
FROM countrylanguage
WHERE isofficial = 'T'
GROUP BY countrycode;

SELECT IsOfficial, COUNT(*)
FROM countrylanguage
GROUP BY IsOfficial;
```

**GROUP BY typisk fejl**

```SQL
SELECT *
FROM City
GROUP BY CountryCode

'Column 'City.ID' is invalid in the select list because it is not contained in either an aggregate function or the GROUP BY clause.'
```

Ovenstående fejl opstår fordi der ikke er noget at tælle på i `GROUP BY Countrycode` der er bare angivet `*` som jo er alt i `City` tabellen og `GROUP BY` skal vide hvilken kolonne der skal bruges.

Det er helt klart muligt at gruppere country code med `GROUP BY` (husk der er flere byer i City tabellen med samme country code)

```SQL
SELECT CountryCode
FROM City
GROUP BY CountryCode;
```

Her vil du få returneret en liste med alle `CountryCode` i `City` tabellen, et udsnit af de 232 rækker i resultatet er vist herunder:

|     | CountryCode |
| --- | ----------- |
| 1   | ISR         |
| 2   | BOL         |
| 3   | CHN         |
| 4   | TGO         |
| 5   | SYR         |
| 6   | WLF         |
| 7   | LBR         |
| 8   | CHL         |
| 9   | SLE         |
| 10  | COL         |
| 11  | AND         |
| 12  | AFG         |
| 13  | YUG         |

Det giver måske mere mening også at inkludere hvor mange byer der er pr. land hvilket kan opnås ved at tælle byerne med aggregate funktionen `COUNT` i `SELECT` udsagnet

```SQL
SELECT CountryCode, COUNT(*) AS CitiesPerCountry
FROM City
GROUP BY CountryCode;
```

Resulatets første 13 linjer, ud af 232 linjer, er vist herunder:

|     | CountryCode | CitiesPerCountry |
| --- | ----------- | ---------------- |
| 1   | ISR         | 14               |
| 2   | BOL         | 8                |
| 3   | CHN         | 363              |
| 4   | TGO         | 1                |
| 5   | SYR         | 11               |
| 6   | WLF         | 1                |
| 7   | LBR         | 1                |
| 8   | CHL         | 29               |
| 9   | SLE         | 1                |
| 10  | COL         | 38               |
| 11  | AND         | 1                |
| 12  | AFG         | 4                |
| 13  | YUG         | 8                |

### HAVING

`HAVING` bruges til at specificere søge betingelser i `GROUP BY` og Aggregate funktioner.
Dokumentationen for ``HAVING` er her:  
[https://docs.microsoft.com/en-us/sql/t-sql/queries/select-having-transact-sql](https://docs.microsoft.com/en-us/sql/t-sql/queries/select-having-transact-sql)

Et eksempel på en forespørgsel der anvender `HAVING` i en tænkt database kan ses herunder:

```SQL
SELECT department_id, SUM(salary)
FROM employees
WHERE department_id > 10
GROUP BY department_id
HAVING SUM(salary) > 10000
```

Fremgangsmåden til at lave forespørgslen er følgende:

1. SELECT vælger kolonnerne
2. FROM vælger hvilken tabel der skal anvendes
3. WHERE filtrerer resultatet
4. GROUP BY delere resultatet op i grupper
5. HAVING filtrerer rækkerne i grupperne

Et eksempel fra World databasen på en forespørgsel efter hvor mange der bor i hver verdensdel der har over 300000000 indbyggere.  
Læg mærke til hvordan `HAVING` bruges til at filtrere region grupperne til resultater over 300000000.

```SQL
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) > 300000000
ORDER BY SumPerRegion DESC;
```

_resultat_

|     | region                    | SumPerRegion |
| --- | ------------------------- | ------------ |
| 1   | Eastern Asia              | 1507328000   |
| 2   | Southern and Central Asia | 1490776000   |
| 3   | Southeast Asia            | 518541000    |
| 4   | South America             | 345780000    |
| 5   | North America             | 309632000    |
| 6   | Eastern Europe            | 307026000    |

**HAVING typisk fejl**

Kig på nedenstående SQL kommando og se om du kan spotte et problem ?

```SQL
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SumPerRegion > 300000000
ORDER BY SumPerRegion DESC;
```

Umiddelbart ser det måske udemærket ud men forespørgslen giver en fejl på 4. linje...

`Msg 207, Level 16, State 1, Line 4 Invalid column name 'SumPerRegion'.`

?? hvorfor det, `SumPerRegion` er jo angivet i `SELECT`.

Grunden er at `HAVING` bliver eksekveret inden `SELECT` og derfor brokker SQL sig og siger at den ikke kender noget til en kolonne ved navn `SumPerRegion`

Løsningen er at lave en aggregate SUM funktion på `HAVING` udsagnet og bruge `population` istedet for `SumPerRegion`:

## Instruktioner

Lav følgende forespørgsler i `World` databasen, husk at bruge dokumentationen til at finde de funktioner du skal bruge

1. Vis totalt overfladeareal pr. verdensdel, sorter faldende
2. Vis forventet levealder stigende i en kolonne, vis lande med den pågældende levealder i en anden kolonne som vist i eksemplet herunder  

    | |LifeExpectancy | CountryNames |
    | --- | --- | --- |
    | 13 |45.2 | Côte d’Ivoire, Ethiopia |  
    | 14 |45.3 | Sierra Leone |
    | 15|45.6 | Guinea |
    | 16|45.9 | Afghanistan |
    | 17|46.0 | East Timor |
    | 18|46.2 | Somalia, Burundi |  
    | 19|46.7 | Burkina Faso, Mali |

3. Vis, i stigende rækkefølge, regioner med en samlet befolkning under 8 millioner

## Links
