---
hide:
  - footer
---

# Opgave 34 - NoSQL databaser

## Information

NoSQL databaser er en fællesbetegnelse for ikke relationelle databaser.  

NoSQL databaser er målrettet store datamængder der varierer meget eller er ustruktureret i sin sammensætning (big data).  

NoSQL benytter et dynamisk/løst skema og implementeres ofte i geografisk distribuerede cloud systemer.  

NoSQL er optimeret til hurtig hentning af data og har ofte bedre performance en relationelle databaser.

Der findes forskellige typer af NoSQL databaser

![images/nosql_types.png](images/nosql_types.png)

Du kender allerede relationelle/SQL databaser så her er en sammenligning af SQL og NoSQL databaser

### SQL vs. NoSQL

||SQL databaser|NoSQL databaser|
|--- | --- | --- |
|**Typer**| En type (SQL database) med få små variationer | Mange forskellige typer, key-value, document databaser, wide-column og graph databaser |
|**Historie**|Udviklet i 1970'erne for at imødekomme de første data storage problematikker | Udviklet i slut 0'erne for at imødekomme udfordringer med SQL databaser. Skalerbarhed, multi struktureret data, geo distribution og agil udvikling hvor ting ændrer sig over tid|
|**Eksempler**|MySQL, PostgreSQL, Microsoft SQL server, Oracle database|MongoDB, Cassandra, HBase, Neo4j, CouchDB|
|**Datalagringsmodel**|Tabeller, rækker og kolonner (regneark). Relateret data gemmes i seperate tabeller og joines efterfølgende|Varierer afhængigt af database type. Dokument databaser gemmer data i et dokument i JSON, XML eller andet format som kan neste data hierakisk|
|**Skema**|Struktur og data typer defineres før data indsættes|Typisk dynamisk, hvor nogle tvinger data validering. Kan tilføje nye felter uden forudgående definition |
|**Skalerbarhed**|Vertical skalering. Når databasen vokser er det nødvendigt at tilføre database serveren flere ressourcer (ram+hdd). Det er muligt at distribuere databasen på flere servere men det giver øget kompleksitet|Horisontal skalering. Kapacitet kan tilføjes ved at tilføje flere servere (on premise eller cloud). Databasen deler automatisk og efter behov databasen på de servere der er til rådighed |
|**Supporterer transaktioner**|Ja, opdateringer i databasen kan konfigureres til at gennemføres eller rulles tilbage|I visse tilfælde og på visse niveauer (f.eks dokument eller database niveau)|
|**Data manipulering**|Bruger et specifikt sprog til SELECT, INSERT, og UPDATE. F.eks. `SELECT fields FROM table WHERE ...`| Bruger objekt orienterede API'er|
|**[Konsistens](https://redis.com/blog/database-consistency/)**|Kan konfigureres til "strong consistency"|Afhænger af database systemet, nogle tilbyder "strong consistency" f.eks. MongoDB. Andre tilbyder "eventual consistency" f.eks Cassandra|   

### MongoDB

MongoDB er en dokument baseret NoSQL database der bruger JSON lignende dokumenter.  
Navnet kommer fra *humongous* og systemet er udviklet i 2007 af MongoDB inc. (tidl. 10gen)  
MongoDB er opensource og gratis
MongoDB bruger dynamiske skemaer som gør integration af data hurtigere og mere simpelt i visse applikationer.  
MongoDB er velegnet til store mængder af ustruktureret/semi-struktureret data.  

Begreberne der anvendes i MongoDB er forskellige fra SQL og kan opsummeres i denne illustration.  

![images/mongo_sql_terms.png](images/mongo_sql_terms.png)

### Tabeller vs. dokumenter

Som beskrevet er MongoDB en dokument baseret database, men hvad betyder det så ?

Hvor relationelle databaser gemmer data som rækker i tabeller, gemmer dokument baserede databaser data som dokumenter i collections.

![images/mongo_sql_tebles_vs_docs.png](images/mongo_sql_tebles_vs_docs.png)

Når data hentes i en SQL database sker det ved at joine data fra forskellige tabeller, det kræver ressourcer fra serveren og tager tid, sammenlignet med at lave en forespørgsel for at hente et dokument fra den dokumentbaserede database.  

Samtidig er skemaet dynamisk og det er muligt at indsætte nye `key:value` par uden at skulle ændre på opsætningen af databasen.

### Collections

Collections er en samling af dokumenter og angives som et array af objekter, dokumenter er selvstændige og kan derfor have forskellige `fields`, altså følger de ikke nødvendigvis er foruddefineret skema.

Internt i den dokumentbaserede database er dokumenter gemt som Binary Javascript Object Notation (BSON) format. 

Læs mere om BSON her: [https://www.mongodb.com/basics/bson](https://www.mongodb.com/basics/bson)

![images/mongo_document.png](images/mongo_document.png)

Nu har du lidt baggrundsviden om dokumentbaserede databaser, men det er kun lige overfladen vi har kradset i.  

Det er altid en god ide at være nysgerrig og researche de emner du bliver introduceret til, på egen hånd.

For eksempel vil det være en god ide at kigge lidt på opbygningen af dokumentationen for MongoDB  
Det kan også betale sig at søge efter gode tutorials på youtube, som f.eks net ninja's complete MongoDB tutorial  

Du skal ikke lade dig begrænse af de ressourcer som du får præsenteret i undervisningen, det kan næsten altid betale sig at finde yderligere ressourcer at studere.  

For hver gang du undersøger og studerer et emne, fra en ny vinkel eller med større baggrundsviden, vil du opnå dybere og mere fundamental viden om emnet.

## Instruktioner

1. Klik dig rundt i MongoDB dokumentation og få et lille overblik over hvordan den er struktureret og hvilke ressourcer der findes 
[https://www.mongodb.com/docs/](https://www.mongodb.com/docs/)
2. Lav et bogmærke for net ninja's complete MongoDB tutorial så du kan se et afsnit en gang i mellem  
[https://youtube.com/playlist?list=PL4cUxeGkcC9h77dJ-QJlwGlZlTd4ecZOA](https://youtube.com/playlist?list=PL4cUxeGkcC9h77dJ-QJlwGlZlTd4ecZOA)
3. Prøv den interaktive mongodb cli på [https://www.mongodb.com/docs/manual/tutorial/getting-started/](https://www.mongodb.com/docs/manual/tutorial/getting-started/)  
Selv om du ikke forstår alt så prøv eksemplerne i de forskellige faner (switch database, Insert, Find all, Filter data, Project fields og aggregate.)


## Links
