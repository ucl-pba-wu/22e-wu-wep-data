---
hide:
  - footer
---

# Opgave 19 - Issue tracker Fysisk ER diagram

## Information

En af jeres kunder har lavet en HTML, CSS og JS skabelon til en issue tracker de skal bruge internt til at håndtere issues i deres udviklingsprojekter.  
De har hørt at i er virkelig gode til at modellere og dokumentere databaser ud fra mockups og de har hyret jer til opgaven.

Det er vigtigt at alle i gruppen deltager i at lave ER diagrammet, det er ofte en fordel at bruge et whiteboard i processen. 
Husk at starte med det konseptuelle ER diagram, så i er enige om de grove træk før i laver for meget arbejde (fail faster strategi)
Brug jeres checkliste fra **Opgave 4 - Database design**

## Instruktioner

1. Lav et fysisk ER diagram der kan understøtte de 2 mockups kunden har leveret
2. Krav der skal opfyldes
    - Issue trackeren, tracker flere projekter
    - En bruger kan være tilknyttet mere end et projekt af gangen
    - Hvert projekt kan have mange issues
    - Et issue oprettes af kun en bruger
    - Et issue relaterer til et projekt
    - Et issue kan have flere labels (css, v4 og så videre)
    - Labels oprettes pr. projekt og tilhører det enkelte projekt, labels har også en farvekode tilknyttet
    - Enhver bruger kan kommentere på et issue

**Mockups**

![images/issuetracker_physical_1.png](images/issuetracker_physical_1.png)  
*Mockup 1: Side med Issue liste*

![images/issuetracker_physical_2.png](images/issuetracker_physical_2.png)  
*Mockup 2: Side til Issue detalje, når der er klikket på et issue*

## Links
