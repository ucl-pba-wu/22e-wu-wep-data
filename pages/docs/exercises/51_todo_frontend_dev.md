---
hide:
  - footer
---

# Opgave 51 - API frontend - udvikling

## Information

Formålet med denne opgave er at udvikle frontend applikationen der interagerer med de forskellige endpoints i jeres API.  
Fokus er at interagere med todo API'en og ikke de grafiske elementer.  
Derfor anbefales det at bruge [bootstrap](https://getbootstrap.com/) eller [materialize.js](https://materializecss.com/) til styling af de grafiske elementer, ellers er opgaven her meget stor.  

I skal bruge javascript `fetch api` til at hente, gemme, ændre, slette og vise data fra jeres todo API.

Fetch API er indbygget i javascript og bruges til at sende HTTP `request` og modtage HTTP `response`.  
Et alternativ til fetch er jquery som ikke er indbygget i javascript.  
Fetch er asynkron og hver request returnerer et `promise` som `resolver` når der modtages et `response`.  

I skal ikke forvente at blive færdige med opgaven her i dagens lektioner, det er den for stor til.  
Grunden til at opgaven er så stor er at der er lang tid til eksamen og i med denne opgave har noget programmering at arbejde med op til eksamen og dermed ikke helt glemmer hvad i har lært.  
Udover eksamen er det altid rart at have et programmerings projekt at hygge sig med, især her i den mørke vintertid :-)

### Fetch GET

Et simplet eksempel på at hente data med fetch kan ses her:  
```js linenums="1"
fetch('<your api url>', {
        method: 'GET'
    }).then(function (response) {
        if (response.status === 200) {
            return response.json();
        } else {
            console.error(`Looks like there was a problem. Status code: ${response.status}`);
            return Promise.reject(response);
        }
    }).then(function (data) {
        // Gør noget med modtaget data
        console.log(data)
        updateList(data)
    }).catch(function (err) {
        console.error("Fetch error: " + err);
    });
```

I eksemplet skal du lægge mærke til at fetch kaldes som en funktion med flere metoder (`.then`, `.catch`) chainet på metoden.  
Fetch kan konfigureres med forskellige ting, f.eks defineres hvilken HTTP metode der skal anvendes, HTTP headers mm. kan også konfigureres her.  

Læg også mærke til `.then` funktionen som kaldes når request returnerer et response objekt.  
Response objektet indeholder en status metode som viser hvilken HTTP status der returneres fra API'en. Hvis response er 200 (OK) returnerer `.then` funktionen response som et nyt promise med data, der først konverteres til json og derefter sendes videre til den næste `.then`, hvis status er noget andet end 200 så udskrives status med `console.error` og promise annuleres.  

Den næste `.then` indeholder det forespurgte data i json format og her kan du gøre noget med det modtagne data, f.eks opdatere data i frontend viewets HTML.

```js linenums="1"
function updateList(data) {
    //hent HTML elementer
    items = data
    items_list_table = document.getElementById("items_list")
    items_list_table.removeChild(document.getElementById("items_list_body"))
    items_list_table.innerHTML += '<tbody id="items_list_body"></ttbody>'
    items_list = document.getElementById("items_list_body")

    // udfyld HTML liste med data
    for (let i = 0; i < data.length; i++) {
        let item_id = data[i]._id//['$oid']
        items_list.innerHTML +=
            '<tr>' +
            '<td hidden>' + item_id + '</td>' +
            '<td>' + data[i].creationDate.substring(0, 10) + '</td>' +
            '<td>' + data[i].title + '</td>' +
            '<td>' + data[i].completed + '</td>' +
            '<td class="center"> <a onclick="updateModal(\'' + item_id + '\')" class="waves-effect waves-light btn-small">Rediger</a> </td>' +
            '<td class="center"> <a onclick="confirmModal(\'' + item_id + '\', \'' + data[i].title + '\', \'' + data[i].completed + '\')" class="waves-effect waves-light btn-small red accent-1">Slet</a> </td>' +
            '</tr>'
    }
};
```

Endelige afsluttes fetch med en `.catch` som fanger eventuelle fetch fejl. 

### Fetch POST

Syntaksen for alle fetch funktioner er meget ens, der er som ofte HTTP metoden der ændres.   
Et eksempel på en Fetch POST request kan ses herunder:

```js linenums="1"
function createTodoItem(item){
    fetch('<your api url>', {
        method: "POST",
        headers: new Headers({
            "content-type": "application/json"
        }),
        body: JSON.stringify(item),
        cache: "no-cache"       
    })
        .then(function (response) {
            if (response.status !== 200) {
                console.error(`Looks like there was a problem. Status code: ${response.status}`);
                return;
            }
            else {
              //alt er ok, HTTP response er 200
            }
        })
        .catch(function (error) {
            console.error("Fetch error: " + error);
        });

}
```

Når der envendes HTTP Post så skal data sendes i `body`. Body defineres som en option til fetch funktionen, i eksempelt er det på linje 7. 

### Eksempel

Fetch kan selvfølgelig bruges til alle HTTP metoder og dermed understøtte fuld CRUD i en frontend applikation.  
Jeg har lavet et eksempel på en funktionel todo frontend applikation som i kan referere til når i laver opgaven.

Eksemplet er tilgængeligt på gitlab [https://gitlab.com/ucl-pba-wu/node-mongo-frontend](https://gitlab.com/ucl-pba-wu/node-mongo-frontend)

Eksemplet kan helt klart refaktoreres så det er mere tydeligt, men uanset hvad vil det nok kræve en del forsøg for at få jeres applikation til at virke.  
Alle javascript funktioner er i filen [actions.js](https://gitlab.com/ucl-pba-wu/node-mongo-frontend/-/blob/main/static/actions.js) og jeg anvender [javascript events](https://www.w3schools.com/js/js_events.asp) til at gøre tryk på knapper osv. funtionelt.  
Min primære argumentation for at anvende events er at jeg helt undgår at genindlæse siden når data skal hentes og opdateres i datatabellen.  


## Instruktioner

**Del 1 - opsætning**

1. Vælg om der skal bruges [bootstrap](https://getbootstrap.com/) eller [materialize.js](https://materializecss.com/) til styling/gui funktionalitet. 
2. Lav et nyt gitlab/github projekt
3. Opret mappestrutur og nødvendige html/javascript filer
4. Initialiser styling framework i html og link til javascript fil(er)
5. Installer [liveserver](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) til at vise siden under udvikling
6. Serv siden og kontroller i din browsers development tools at der ikke er nogen fejl når siden loades 

![images/debug_console_no_errors.png](images/debug_console_no_errors.png)

**Del 2 - basis CRUD funktionalitet**

1. Start helt simpelt med at lave en javascript funktion der henter alt data fra jeres API og opretter samt viser data i en HTML tabel.   
2. Udvid med en HTML form til at oprette et nyt todo item og gemme det via API'en.
3. Når et nyt todo item er oprettet skal data hentes fra api på ny og tabellen opdateres
4. Implementer at et todo item kan slettes med via jeres API delete endpoint.     
5. Implementer at et todo item kan opdateres via jeres API put/patch endpoint.     

**Del 3 - udvidet funktionalitet**

1. Implementer søgefunktionen på siden. Her skal du lave et `text index` i din mongodb database så du kan lave en [full-text search](https://www.mongodb.com/basics/full-text-search) og du skal lave et nyt endpoint i din API til søgning f.eks your_api_url/tasks/search/:searchQuery.  
I er velkomne til at se hvordan jeg har lavet search endpointet i min api [https://gitlab.com/ucl-pba-wu/node-mongo-todo](https://gitlab.com/ucl-pba-wu/node-mongo-todo)
2. Implementer validering på formen der anvendes til at oprette og opdatere et todo item så brugeren oplyses om felter der skal udfyldes. Det skal ikke være muligt at "submitte" før formen er ok.
3. Implementer "pagination" så der kun vises 10 resultater pr. side, her er det ok at bruge f.eks [https://materializecss.com/pagination.html](https://materializecss.com/pagination.html) eller [https://getbootstrap.com/docs/4.0/components/pagination/](https://getbootstrap.com/docs/4.0/components/pagination/)
4. Implementer sortering af resultater i stigende/faldende rækkefølge baseret på `creationDate`  

## App screen shots

### App vist med data

![images/todo_frontend.png](images/todo_frontend.png)

### Form til at oprette nyt todo item

![images/todo_create_form.png](images/todo_create_form.png)

### Form til at opdatere todo item (med data fra db)

![images/todo_update_form.png](images/todo_update_form.png)

### Søgning med full text search i mongodb

![images/todo_search.png](images/todo_search.png)

### Sortering efter dato faldende/stigende

![images/todo_sort.png](images/todo_sort.png)

### Form validering

![images/todo_validation.png](images/todo_validation.png)

### Pagination med 10 resultater pr. side

![images/todo_pagination.png](images/todo_pagination.png)

## Links

- Fetch api dokumentation [https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)