---
hide:
  - footer
---

# Opgave 45 - mongodb node drivers

## Information

Du skal i denne opgave bygge videre på `node_mongo_todo` applikationen som du startede på i **Opgave 44 - express.js**.  

Med denne opgave bygger du et "proof of concept" for en web API, hvor du viser at du kan hente data fra en MongoDB database med Node.js, mongoose og express.js

Du bygger nedenstående arkitektur

![images/api_web_architecture.png](images/api_web_architecture.png){ width=400 }

For at kunne kommunikere med en MongoDB server/database fra en node appliaktion er det nødvendigt at bruge en driver.

Der findes mere end en driver

- Den officielle node MongoDb driver som anvender en syntaks der er meget tæt på den syntaks du kender fra mongosh [https://www.mongodb.com/docs/drivers/node/current/](https://www.mongodb.com/docs/drivers/node/current/)  
Det er den som net ninja anvender i sin playliste. 
- Mongoose som er en ODM (Object Document Mapper). Mongoose har den styrke at du ikke bruger mongo syntaks men i stedet kan arbejde direkte med javascript objekter [https://mongoosejs.com/](https://mongoosejs.com/)
Mongoose anvender også dokument modeller som sikrer at data gemmes i det korrekte format i databasen.

ODM er en MongoDB term som stammer fra ORM der står for Object Relational Mapper. En ORM bruges til at kommunikere med relationelle databaser der bruger SQL.

Der er både fordele og ulemper ved at bruge en ORM/ODM og du kan læse om dem her  
[https://blog.bitsrc.io/what-is-an-orm-and-why-you-should-use-it-b2b6f75f5e2a](https://blog.bitsrc.io/what-is-an-orm-and-why-you-should-use-it-b2b6f75f5e2a)

Fordi net ninja allerede dækker brugen af den officiele MongoDB driver så dækker jeg brugen af Mongoose ODM.

Her følger et par eksempler på brugen af mongoose

## Eksempler på brug af mongoose

Du kan og bør bruge dokumentationen når du arbejder med mongoose, du kan finde den her  
[https://mongoosejs.com/docs/guides.html](https://mongoosejs.com/docs/guides.html)

### Forbind til en mongodb database

```js linenums="1"
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground', { useNewUrlParser: true })
 .then(() => console.log('Connected…'))
 .catch(err => console.error('Conn. failed!' + err));
```

### Definér et dokument skema

Dokument skemaer bestemmer hvordan MongoDB dokumenter er formet. De kan også bruges til at sikre at data er korrekt med validering.  
Skemaer giver også mulighed for at tilføje standard (default) værdier til felter 

```js linenums="1"
const courseSchema = new mongoose.Schema({
  name: { type: String, required: true},
  price: Number,
  isPublished: { type: Boolean, default: false}
});
```

### Opret en model fra et skema

```js linenums="1"
const Course = mongoose.model('Course', courseSchema);
```

### Lav et objekt ud fra en model

```js linenums="1"
const courseA = new Course({
  name: 'DB med Nikolaj',
  price: 0 
})
```

### Gem objekt i database

```js linenums="1" 
Course.create(courseA, function (err) {
  if (err) return console.log(err.message);
  console.log("Doc saved to DB");
});
```

### Andre mongoose funktioner

Der findes selvfølgelig flere metoder i mongoose, brug dokumentationen til at finde flere eksempler

  ```js
  Course.find(...)
  Course.aggregate(...)
  etc.
  ```

## Instruktioner

**Alt kode skal du skrive og ikke kopiere** 
Hvis du kopierer koden lærer du absolut ingenting!  
Programmering er et håndværk.  

- Ved selv at skrive koden lærer du dine værktøjer at kende
- Du får trænet muskel hukommelsen til at anvende tastatur og syntaks
- Du husker bedre det du har lavet
- Du kan bedre finde rundt i kode og filer når du skal fejlfinde 
- Du bliver bedre til at læse og forstå kode  
-----------

1. Installer mongoose i `node_mongo_todo` applikationen fra **Opgave 44 - express.js** lokalt.  
`npm install mongoose --save`
2. Lav en mappe der hedder controllers og i den mappe en fil der hedder `TaskController.js`
3. I `TaskController.js` **skriv** følgende javascript kode
  ```js linenums="1"
  const express = require("express");
  const router = express.Router();

  router.use(express.json());

  // CREATE A NEW TASK
  router.post("/", function(req, res) {
  res.send("create new task");
  });

  // RETURN ALL THE TASKS IN THE DATABASE
  router.get("/", function(req, res) {
  res.send("return all tasks");
  });

  module.exports = router;
  ```
4. Ret `app.js` filen til
  ```js linenums="1"
  const express = require("express");
  const app = express();
  const port = 3000;

  const TaskController = require("./controllers/TaskController");

  app.use("/tasks", TaskController);

  app.listen(port, () => console.log(`Example app listening on port ${port}!`));
  ``` 
5. Lav en fil med navnet `db.js`. Den skal være placeret i samme mapp som `app.js`
6. I `db.js` **skriv** følgende javascript kode
  ```js linenums="1"
  const mongoose = require('mongoose');

  mongoose.connect('mongodb://127.0.0.1:27017/taskdb')
  .then(() => {
      console.log('db connected...');
  })
  .catch(err => console.log(err));
  ```
7. Importér `db.js` i `app.js` lige efter linje 3
  ```js
  const db = require("./db");
  ```
8. Lav en mappe og kald den `models`
9. I models mappen lav en fil og kald den `Task.js`. **Skriv** følgende javascript kode i `Task.js`
  ```js linenums="1"
  const mongoose = require("mongoose");

  const TaskSchema = new mongoose.Schema({
    title: { type: String, required: true },
    creationDate: { type: Date, default: Date.now },
    completed: { type: Boolean, default: false }
  });

  const Task = mongoose.model("Task", TaskSchema);

  module.exports = Task;
  ```
10. I filen `TaskController.js` importer task modellen på linje 3 ved at **skrive**
  ```js
  const Task = require('./../models/Task');`
  ```
11. I filen `TaskController.js` Tilføj funktionalitet til dine eksisterende endpoints ved at **skrive**
  ```js linenums="1"
  // CREATES A NEW TASK
  router.post("/", function(req, res) {
    Task.create(
    {
      title: req.body.title,
      completed: req.body.completed
    },
    function(err, task) {
      if (err)
        return res
          .status(500)
          .send("There was a problem adding the information to the database.");
      res.status(200).send(task);
      }
    );
  });

  // RETURNS ALL THE TASKS IN THE DATABASE
  router.get("/", function(req, res) {
    Task.find({}, function(err, tasks) {
      if (err)
        return res.status(500).send("There was a problem finding the tasks.");
      res.status(200).send(tasks);
      });
  });
  ```
12. Kør applikationen med kommandoen `nodemon app.js` og kontroller at applikationen lytter på port 3000 og forbinder til databasen (*db connected...* skrives i terminalen når database serveren er forbundet)
13. Brug Postman til at oprette et dokument i databasen. Du skal vælge `POST` og bruge body > raw > JSON og så skrive dit json dokument der. 
URI er `127.0.0.1:3000/tasks`
14. Brug postman til at hente alle dokumenter i databsen, pt. er det kun det du lige har oprettet.
15. Test hvad der sker hvis du ikke sender korrekt json, hvilken fejl får du ? Er det den der er beskrevet i `Task.create()` ? 
16. Brug MongoDB compass til at kontroller at din database er oprettet og at der er dokumenter i den.
17. Slet alle dokumenter i databasen med MongoDB compass og afprøv om du får den forventede fejlmeddelelse som skrevet i `Task.find()`
18. Opret et par dokumenter mere og hent dem, bare for at bliver fortrolig med hvordan applikationen opfører sig

**Fejlfinding**

Fejlfinding er utrolig lærerigt, at rette fejl er en mulighed for at lære hvordan noget fungerer :-)

1. Hold øje med den indbyggede terminal i VS Code, den giver sammen med nodemon besked når der er fejl, husk at læse fejlbeskederne!
2. Har du **skrevet** det hele rigtigt, er der alle symboler med og har du de rigtige linjeskift ?? Syntaksfejl er de fejl der oftest sker.
3. Problemer med at forbinde til databasen? Prøv at starte MongoDB Atlas og forbind med applikationen igen, nogen gange er det som om at mongodb servicen ikke reagerer
4. Søg på nettet efter specifikke fejlmeddelser
5. Brug dokumentationen til det der fejler
6. Spørg din underviser og dine medstuderende om de oplever den samme fejl som dig
7. Hjælp andre, du lærer også meget ved at forklare din viden
8. Din mappe/fil struktur i applikationen bør se ud som følgende:
```
mongo-todo/
├── app.js
├── controllers
│   └── TaskController.js
├── db.js
├── models
│   └── Task.js
├── node_modules
├── package.json
└── package-lock.json
```

## Links
