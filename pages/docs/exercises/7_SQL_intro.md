---
hide:
  - footer
---

# Opgave 7 - SQL introduktion

## Information

Opgaven er en gruppe opgave og i skal samlet notere jeres svar så hele gruppen har adgang til svarene.

SQL er en forkortelse af Structured Query Language og er et deklarativt programmeringssprog, der kort fortalt betyder at instruktioner er kendetegnet ved at du skriver hvad du vil have programmet til at gøre og ikke hvordan det skal gøres.
Det gør syntaksen relativt simpel og let at læse.

SQL implementationer varierer en lille smule mellem forskellige database management systemer, det på trods af at SQL er [standardiseret](https://blog.ansi.org/2018/10/sql-standard-iso-iec-9075-2016-ansi-x3-135/)

Den implementering vi anvender i dette fag er Microsoft's og hedder Transact SQL (T-SQL). Det omtales dog også som MSSQL og Microsoft SQL.

SQL har 3 hovedansvarsområder:

- Oprettelse af databasen og definering af dens struktur ([DDL - Data Definition Language](https://en.wikipedia.org/wiki/Data_definition_language))
    ```sql title="DDL SQL kommando eksempler"
    CREATE
    DROP
    ALTER    
    ```
- Forespørgsler til, samt opdatering af datasen ([DML - Data Manipulation Language](https://en.wikipedia.org/wiki/Data_manipulation_language))
    ```sql title="DML SQL kommando eksempler"
    SELECT
    INSERT
    UPDATE
    DELETE
    ```
- Kontrol af database sikkerhed ([DCL - Data Control Language](https://en.wikipedia.org/wiki/Data_control_language))
    ```sql title="DCL SQL kommando eksempler"
    GRANT
    REVOKE 
    ```

Herunder er et eksempel på SQL og en forklaring på de forskellige dele af kommandoen.

![images/sql_statement_example.png](images/sql_statement_example.png)

SQL tvinger ikke brug af store og små bogstaver i syntaksen men det anbefales at bruge STORE bogstaver til nøgleord og hver delsætning på sin egen linje for at sikre læsbarhed.

## Instruktioner

1. Læs om T-SQL navngivnings og formatterings konventioner her:  
    [https://docs.microsoft.com/en-us/sql/t-sql/language-elements/transact-sql-syntax-conventions-transact-sql](https://docs.microsoft.com/en-us/sql/t-sql/language-elements/transact-sql-syntax-conventions-transact-sql) 

    **_besvar følgende:_**  
    - Hvordan håndteres navne med flere ord ?  
    - Hvilke ord skal skrives med STORT ? (giv gerne eksempler)

2. Læs om `CREATE DATABASE` kommandoen i microsofts T-SQL Dokumentation [https://docs.microsoft.com/en-us/sql/t-sql/statements/create-database-sql-server-transact-sql](https://docs.microsoft.com/en-us/sql/t-sql/statements/create-database-sql-server-transact-sql)  

    **_Besvar følgende:_**  
    - Hvad gør kommandoen ?  
    - Hvad er syntaksen ? (skriv et eksempel)

3. Læs om `CREATE TABLE` kommandoen i microsofts T-SQL Dokumentation [https://docs.microsoft.com/en-us/sql/t-sql/statements/create-table-transact-sql](https://docs.microsoft.com/en-us/sql/t-sql/statements/create-table-transact-sql)  

    **_Besvar følgende:_**  
    - Hvad gør kommandoen ?  
    - Hvad er syntaksen ? (skriv et eksempel)

## Links

- Transact SQL reference [https://docs.microsoft.com/en-us/sql/t-sql/language-reference](https://docs.microsoft.com/en-us/sql/t-sql/language-reference)