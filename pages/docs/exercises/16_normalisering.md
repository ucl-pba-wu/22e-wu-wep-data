---
hide:
  - footer
---

# Opgave 16 - Normalisering

## Information

Begrebet *Normalisering* i en database kontekst er et sæt regler der skal sikre at databasens relationer er entydige og at data ikke gentages i databsens tabeller.

*Normalisering* har 8 niveauer som alle er afhængige af hinanden, det vil sige at en database der opfylder reglerne for 3. normal form også opfylder 1. og 2. normalform.

Du kan se en oversigt over normalformerne her: [https://web.archive.org/web/20080805014412/http://www.datamodel.org/NormalizationRules.html](https://web.archive.org/web/20080805014412/http://www.datamodel.org/NormalizationRules.html)

Ofte fører normalisering af en database til en yderligere opdeling af tabellerne, målet er at gøre det muligt at forespørge og opdatere effektivt og entydigt i databasen.

For det meste er 3. normalform et tilstrækkeligt normaliseringsniveau.

Hvis du praktiserer god **Entity Relation (ER)** modellering med korrekte relationer vil du ofte enden med et resultat der er i 3. normalform.
Derfor er denne opgave mere en teori øvelse der oplyser dig om eksistensen af normalisering samt hvad de enkelte regler indeholder. 

Her er de første 3 normaliserings former beskrevet. 

1. Normalform (NF1)
  – De enkelte felter må kun indeholde én værdi.
  – Der må ikke være kolonner som gentager sig.
  - Hver række har en primary key (kan være en kombination af flere felter/komposite key)

En composite/compound key er en kombination af 2 eller flere kolonner. Composite key vil på den måde kun være unik når kolonnerne er kombineret, ikke hvis kun en af kolonnerne benyttes. 

**NF1 Eksempel**

I nedenstående illustration overholdes 1. Normalform ikke fordi der er flere værdier i *Uses* kolonnen
![images/1NF_1.png](images/1NF_1.png)

I nedenstående illustration overholdes 1. Normalform ikke fordi der er gentaget data i *Use1, Use2, Use3* kolonnerne
![images/1NF_2.png](images/1NF_2.png)

Løsningen er at splitte tabellen i 2 tabeller og bruge PlantID som foreign key i Uses tabellen
![images/1NF_3.png](images/1NF_3.png)

2. Normalform 2 (NF2)
  - Overholder NF1
  - Alle felter der ikke er en Key skal være afhængige af hele Primary Key

**NF2 Eksempel 1**

Illustrationen viser at tabellen overtræder 2. normalform, fordi Use kolonnen kun er afhængig af af en del af PK (UseID)
![images/2NF_1.png](images/2NF_1.png)

Løsningen er at dele Uses tabellen i 2 tabeller og bruge UseID som foreign key i PlantUses tabellen
![images/2NF_2.png](images/2NF_2.png)

**NF2 Eksempel 2**

projectName afhænger kun af en del af PK (compound key) 
![images/2NF_3.png](images/2NF_3.png)

Løsningen er at dele tabellen og bruge projectID som foreign key.
![images/2NF_4.png](images/2NF_4.png)

3. Normalform 3 (NF3)
  - Overholder NF2
  - Hvis der er mere end et felt der kan sættes som nøgle for andre felter, skal tabellen opdeles i flere.

**NF3 Eksempel**

DepName er ikke afhængig af primary key (EmpID)
![images/3NF_1.png](images/3NF_1.png)

Løsningen er at splitte i 2 tabeller og bruge DeptNum som foreign key
![images/3NF_2.png](images/3NF_2.png)

## Instruktioner

1. Læs denne artikel om database normalisering [https://web.archive.org/web/20080805014412/http://www.datamodel.org/NormalizationRules.html](https://web.archive.org/web/20080805014412/http://www.datamodel.org/NormalizationRules.html) fokuser på de 3 første former
2. Kig det fysiske ER diagram fra **Opgave 15 - Ejendomsmægler fysisk ER diagram** igennem og diskuter om det opfylder 3NF ?  
Hvis ikke, så ret det til, så det er i 3NF :-) 

## Links
