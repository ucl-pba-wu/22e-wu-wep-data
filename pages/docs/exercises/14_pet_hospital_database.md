---
hide:
  - footer
---

# Opgave 14 - Pet hospital database

## Information

I denne opgave skal i oprette databasen til Pet hospital samt indsætte data i databasens tabeller.

Opgaven er en individuel opgave.
Det er selvfølgelig en god ide at sidde sammen når i arbejder så i kan hjælpe hinanden. 

Data kommer fra et dyre hospital og jeres opgave er at oprette en database på en Microsoft SQL Server der kan erstatte hospitalets excel ark.

Heldigvis har vi fået et eksternt konsulent firma til at lave det fysiske ER diagram for os.

![images/pethospital-01.png](images/pethospital-01.png)

Når du er førdig med opgaven bør du have 3 SQL script filer, en for hvert punkt i instruktionerne.

## Instruktioner

1. Opret databasen og tilhørende tabeller inklusiv Primary Keys og Foreign Keys i et seperat SQL script `.sql`
2. Find på data og indsæt data i databasens tabeller. Det skal gøres i i et seperat SQL script.
3. Lav 3 forespørgsler i databasen efter eget valg, igen i et seperat SQL script.


## Links
