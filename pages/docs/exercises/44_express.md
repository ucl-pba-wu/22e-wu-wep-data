---
hide:
  - footer
---

# Opgave 44 - express.js

## Information

Express.js er et web framework der gør udvikling af web servere forholdsvist nemt i node.js.
Express.js installeres som en npm pakke med kommandoen `npm install express --save` og gemmes som en dependency (afhængighed) i dit node projekts `package.json` fil.  

I en express node appikation er den generelle opbygning af du definerer endpoints for din web applikation, i vores tilfælde vil det være API endpoints.  
For hver endpoint defineres HTTP metoden og stien til endpointet f.eks 
```js
app.get('/', (req, res) => {
    // console.log(req)
    // console.log(res)
    res.send('Hello World!')
})
```
`app` er et objekt der er instatieret til express, `get` er http metoden, `/` er stien, `(req, res)` er argumenter til en anonym funktion der kan bruges i et callback når `app.get` funktionen køres og endelig er `res.send('Hello World!'))` det svar der sendes tilbage til klienten.  
I eksemplet sendes bare en simpel tekststreng til browseren, men det kunne også være html 

```js
app.get('/', (req, res) => {
    // console.log(req)
    // console.log(res)
    res.send('<h1>Hello World!</h1>')
})
```

eller json der blev sendt tilbage

```js
app.get('/', (req, res) => {
    // console.log(req)
    // console.log(res)
    res.json({ 
      "message": "Hello World",
      "answer": 42
     })
})
```

`req` er faktisk et objekt der indeholder den fulde http forespørgsel, altså det der sendes fra din browser og modtages i applikationen, `res` er også et objekt og indeholder det fulde http svar der sendes tilbage til browseren

Jeg har inkluderet 2 console log's som er udkommenteret, `req` og `res` objekterne er temmelig store og hvis du vil se hvad de indeholder der du indkommentere `console.log` linjerne, men tag en af gangen fordi det er store objekter.

Dokumentation for express frameworket kan findes her [https://expressjs.com/](https://expressjs.com/)

## installation af npm pakke

Der findes et par måder at installere npm pakker, lokalt (for projektet), globalt (for din computer) og som development dependency.

- lokal installation er til de pakker som bruges til at afvikle applikationen, f.eks [express](https://expressjs.com/)
- global installation er for pakker du skal bruge på tværs af node projekter, f.eks [nodemon](https://www.npmjs.com/package/nodemon)
- Development dependency er pakker som udelukkende er nødvendige til udvikling af applikationen, f.eks [webpack](https://www.npmjs.com/package/webpack)

Pakker kan selvfølgelig også afinstalleres.

Herunder er eksempler på installation og afinstallation af npm pakker

```js
// Install/uninstall local package 
npm install <package_name> 
npm uninstall <package_name>

// Install local package as dev dependency
npm install <package_name> --save-dev

// Install/uninstall global package
npm install -g <package_name> 
npm uninstall -g <package_name>
```

## require

Når du skal bruge pakker fra npm så skal du importere dem i din fil. Det gøres med `require`.
Når du importerer en pakke så får du adgang til pakkens funktionalitet i din fil.

Du kan både importere pakker/moduler du selv har skrevet, andre har skrevet eller moduler som er indbygget i node.js.

Hvis du skal importere f.eks express kan du i toppen af din `.js` fil skrive:

```js
const express = require('express');
```
Her instantieres en konstant variabel til at indeholde `express` modulet. 

require søger efter moduler i denne rækkefølge

1. indbyggede node.js moduler 
2. NPM moduler i node_modules mappen
3. Lokale moduler som matcher filtyperne `*.js`, `*.json`, `*.mjs`, `*.cjs`, `*.wasm` og `*.node`.  
Hvis  `./`, `/` eller `../` skrives foran modulnavnet leder node efter modulet i den angivne mappe/sti.

Læs mere om at bruge node moduler her [https://adrianmejia.com/getting-started-with-node-js-modules-require-exports-imports-npm-and-beyond/](https://adrianmejia.com/getting-started-with-node-js-modules-require-exports-imports-npm-and-beyond/)

## Instruktioner

1. Lav en mappe til et node.js projekt, kald mappen `node_mongo_todo`, her gemmes alle filer og undermapper der hører til projektet 
2. Lav et nyt node.js projekt i mappen med `npm init`
3. Installer express.js med kommandoen `npm install express --save` --save betyder at express gemmes som en dependency
Når du første gang installerer en pakke i et node projekt oprettes mappen `node_modules` hvor indholdet af pakken gemmes. 
`package.json` bliver også opdateret med express som dependency
```js linenums="1"
"dependencies": {
  "express": "^4.18.2"
}
```
4. Lav en fil der hedder `app.js` 
5. I app.js tilføj (hvis du skriver i stedet for copy/paste lærer du mere og husker bedre!)
```js linenums="1"
const express = require('express');
const app = express();
const port = 3000;

app.listen(port, () => 
    console.log(`todo app is listening on port ${port}`)
    )

app.get('/', (req, res) => {
    // console.log(req)
    // console.log(res)
    res.send("Hello World")
})
```
6. gem filen og start web serveren med `node app.js`
7. Start din browser og gå til adressen `http://127.0.0.1:3000/`
8. Hvis din web server virker bør der nu stå `Hello World` i dit browser vindue
9. Ret `app.get` funktionen så der returneres html istedet for tekst og test at det virker (du skal genstarte node applikationen for at ændringer slår igennem)
10. Ret i din applikation så du bruger port 4000 i stedet for port 3000 og test
11. Det bliver hurtigt irriterende at aplikationen skal genstartes hver gang den rettes, heldigvis findes [nodemon](https://www.npmjs.com/package/nodemon) som genstarter serveren hver gang ændringer gemmes.  
Installer nodemon globalt med `npm install -g nodemon` og check om det er installeret med `nodemon --version`
12. Start nu din applikation/server med `nodemon app.js` 
13. Ret `app.get` funktionen så der returneres json istedet for tekst og test at det virker

## Links
