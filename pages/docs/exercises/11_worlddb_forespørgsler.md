---
hide:
  - footer
---

# Opgave 11 - WorldDB forespørgsler

## Information

I denne opgave skal du bruge "WorldDB" databasen til at træne simple SQL forespørgsler.  
Du lærer også at samle dine SQL sætninger i en SQL script fil.  

Hvis du allerede har installeret WorldDB tidligere kan du springe trin 1-3 over.

Her kan du se et ER diagram over WorldDB databasen:

![images/worlddb_schema.png](images/worlddb_schema.png)

## Instruktioner

1. Download **worldDB.zip** fra itslearning, pak zip filen ud
2. Åbn filen i SSMS/AzureDataStudio
3. Eksekver filen ved hjælp af Execute/Run i query vinduet
4. Hvis du bruger SSMS kan du læse om hvordan du laver forespørgsler her [https://docs.microsoft.com/en-us/sql/ssms/f1-help/database-engine-query-editor-sql-server-management-studio?view=sql-server-ver16](https://docs.microsoft.com/en-us/sql/ssms/f1-help/database-engine-query-editor-sql-server-management-studio?view=sql-server-ver16)
5. Hvis du bruger Azure Data Studio (anbefalet, også på windows) skal du forbinde til din database server, udvide *databases*, højreklikke på *World* og vælge *New Query*  
  ![images/ADS_new_query.png](images/ADS_new_query.png)
6. Test at det virker ved at vise alt indhold i `City` tabellen med SQL sætningen:  
  ```SQL
  SELECT * 
  FROM City;
  ```
  Resultatet skal se ud som dette screenshot:  
  ![images/worlddb_query.png](images/worlddb_query.png)
Læs om `.sql` scripts [https://www.sqlshack.com/learn-sql-sql-script/](https://www.sqlshack.com/learn-sql-sql-script/)
7. Lav en ny fil fra `file` menuen i Azure Data Studio
8. Gem den som `worlddb_queries.sql` via `file` menuen
9. Skriv dine forespørgsler som seperate statements i filen, husk at få spørgsmålet med, se nedenstående eksempel:
  ```sql
  /* Show all cities starting with 'X' */
  
  SELECT *
  FROM City
  WHERE name LIKE 'X%';
  ```
10. lav følgende forespørgsler via AzureDataStudio i `worlddb_queries.sql` filen:
    1. Vis alt indhold i `Country` tabellen
    2. Vis bynavn og landekode for alle byer i Holland (NLD)
    4. Vis alle regioner i Europa, sørg for der ikke er nogle dubletter
    1. Find alle byer i Hubei distriktet der har et navn der starter med X
    2. Vis en top 10 over byer med flest indbyggere, kun navn og befolkningstal skal vises
    3. Vis alle byer i La Paz, West Java og Limburg, sorter efter A-Z
    5. Vis landenavne med et areal under 100 og en forventet levealder på mere end 80
    6. Vis alle lande der ikke har nogen HeadOfState
    7. Vis landekoden for alle lande hvor engelsk er det officielle sprog
    8. Lav en forespørgsel selv inkl. et spørgsmål som ovenstående

**Husk løbende at afprøve dine forespørgsler. Hvis du vil eksekvere et enkelt statement kan du highlighte det med musen og klikke run.**

## Links

Hvis du har lyst til at træne flere forespørgsler på andre databaser. Kan du finde flere øvelser her [https://www.w3resource.com/sql-exercises/](https://www.w3resource.com/sql-exercises/)