---
hide:
  - footer
---

# Opgave 8 - Installer MSSQL og SSMS

## Information

For at kunne arbejde med Micrsoft SQL databaser på din computer, skal du anvende en SQL server som kan indeholde dine databaser.
For at kunne tilgå databaserne på SQL serveren, skal du anvende et database management system.

Denne opgave guider dig igennem installation af både Microsoft SQL Server (MSSQL) og Microsoft SQL Server Management Studio (SSMS) på windows.

I skal arbejde i grupper så i kan hjælpe hinanden med installationen. I må meget gerne blive i klassen så jeg har mulighed for at hjælpe jer hvis i ikke kan få det til at virke. Det er vigtigt at alle har begge programmer installeret, i skal bruge det resten af semestret.

## Instruktioner

**_Windows_**

1. Hent installations filen til MSSQL, du skal vælge Express versionen: [https://www.microsoft.com/en-us/sql-server/sql-server-downloads](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)
2. Installer MSSQL express og vælg **Basic** under installationen.
3. Når installationen er færdig (det tager lidt tid) så vises et vindue med MSSQL fil lokationer, tag et screenshot af det vindue og gem det til evt. senere brug
   ![images/ms_sql_server_info_E22.png](images/ms_sql_server_info_E22.png)
4. Klik på **Install SSMS** og download [SQL Server Management Studio](https://aka.ms/ssmsfullsetup)
5. Installer SSMS
6. Når installationen er færdig bliver du spurgt om du vil forbinde til din database server, her skal du vælge ja.
7. Hent filen `world_db.zip` på its learning og pak den ud
8. Åbn worlddb.sql i SSMS/AzureDataStudio og tryk Execute/Run
9. Bekræft at du nu har en database der hedder **World**
10. klik lidt rundt i databasens mapper og se hvilke tabeller etc. der findes

**_OSX_**

Hvis du bruger OSX skal du installere MSSQL Server i docker.

1. Installer Docker [https://store.docker.com/editions/community/docker-ce-desktop-mac](https://store.docker.com/editions/community/docker-ce-desktop-mac)
2. Efter installation skal du åbne et terminal vindue og skrive:
   ```
   docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest
   ```
   **yourStrong(!)Password** skal udskiftes med et password du vælger (husk at gemme det!)
3. Installer Azure Data Studio [https://docs.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio](https://docs.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio)
4. I Azure Data Studio skal du åbne en connection til _localhost,1433_  
   ![images/azure_data_studio_conn.png](images/azure_data_studio_conn.png) username er `sa` og password det du valgte i trin 2.
5. Hent filen `world_db.zip` på its learning og pak den ud
6. Åbn worlddb.sql i SSMS/AzureDataStudio og tryk Execute/Run
7. Bekræft at du nu har en database der hedder **World**
8. klik lidt rundt i databasens mapper og se hvilke tabeller etc. der findes

**_Linux_**

Bruger du Linux kan du finde installations guides på [https://www.microsoft.com/da-dk/sql-server/sql-server-downloads?rtc=1](https://www.microsoft.com/da-dk/sql-server/sql-server-downloads?rtc=1) nederst på siden.

Efter installation:  
1. Hent filen `world_db.zip` på its learning og pak den ud
2. Åbn worlddb.sql i SSMS/AzureDataStudio og tryk Execute/Run
3. Bekræft at du nu har en database der hedder **World**
4. klik lidt rundt i databasens mapper og se hvilke tabeller etc. der findes

## Links

- [https://hub.docker.com/\_/microsoft-mssql-server](https://hub.docker.com/_/microsoft-mssql-server)
