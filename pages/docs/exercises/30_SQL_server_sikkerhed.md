---
hide:
  - footer
---

# Opgave 30 - SQL server sikkerhed

## Information

Sikkerhed er et stort emne og er mere aktuelt end nogensinde.  

Vi hører dagligt om angreb på danske og udenlandske virksomheder.  

Motivationen hos angribere kan være mange, fra aktivisme (anonymous) til statsfinansierede grupper (Lazarus/Nord korea, REvil/Rusland).  

De fleste angreb udføres dog af kriminelle med profit som motivation.      

### Ransomware

Et af de helt store emner er pt. ransomware angreb.  

Kort fortalt er ransomware et begreb der dækker over at en virksomheds data bliver stjålet og samtidig krypteret(låst) så virksomheden ikke har adgang til data mere.  

Data er som regel følsom information om kunder og ansatte, forretningshemmeligheder, brugernavne, passwords, mails, regnskaber og andet der kan skade virksomheden hvis de bliver offentliggjort.  

De kriminelle kræver derefter en ransom (løsesum) for at sende nøglen der kan dekryptere data. De lover også at data ikke bliver offentliggjort hvis løsesummen betales (det er ikke altid de overholder det)  

IT kriminalitet er i dag business as usual forstået på den måde at professionelle it kriminelle er organiseret som enhver anden virksomhed med forskellige afdelinger.  
Produktet de sælger er Ransomware As A Service (Raas) som dels er noget "kunder" udefra kan bestille,det vil sige at de kan købe sig til et angreb.  
Men ofte fremstiller grupperne sig som "sikkerhedstestere" der udfører en "service" for de virksomheder de angriber, argumentet er at de hjælper virksomhederne med at teste deres sikkerhed, godt nok uden at virksomheden har bedt om det..... 

Du kan læse mere om ransomware truslen i denne microsoft artikel [https://www.microsoft.com/security/blog/2022/05/09/ransomware-as-a-service-understanding-the-cybercrime-gig-economy-and-how-to-protect-yourself/](https://www.microsoft.com/security/blog/2022/05/09/ransomware-as-a-service-understanding-the-cybercrime-gig-economy-and-how-to-protect-yourself/)  

### Hvordan foregår et angreb ?

Kriminelle har brug for en åbning i en virksomheds systemer for at kunne opnå adgang.  
Når de har fundet en åbning udnytter de den til at opnå administrator/root rettigheder og kan derefter gøre hvad de vil på systemet.  

Åbninger skyldes ofte forældet software med kendte eller ukendte sikkerheds huller.  
Ukendte sikkerheds huller kaldes også for *0day*, fordi der er 0 dage til at rette sårbarheden.   
Software er alt fra Server software, firmware i netværksudstyr og applikationer virksomheden selv udvikler.  

Kendte sikkerhedshuller offentligøres som det der hedder *Common Vulnerabilities and Exposures*(CVE) [https://www.redhat.com/en/topics/security/what-is-cve](https://www.redhat.com/en/topics/security/what-is-cve)  

Der findes adskillige CVE databaser, en af dem vedligeholdes af den amerikanske organisation NIST(National Institute of Standards and Technology).  

Her kan du f.eks finde en SQL server sårbarhed fra 2022 der giver en angriber mulighed for at eksekvere kode på SQL Serveren [https://nvd.nist.gov/vuln/detail/CVE-2022-29143](https://nvd.nist.gov/vuln/detail/CVE-2022-29143)  

Det er almindeligt at sikkerheds researchere laver det der hedder et proof of concept(POC) på kendte sårbarheder, POC er ofte et stykke kode(exploit) der kan udnytte sårbarheden.
Der findes websider med databaser over exploits som f.eks [https://www.exploit-db.com](https://www.exploit-db.com)

Et angreb foregår i grove træk efter nedenstående punkter:

1. Rekognosering af målet (offentlig tilgængelig data)
2. Kortlægning af de teknologier målet anvender
3. Søgning efter sårbarheder hos målet
4. Udnyttelse af sårbarheder (exploitation)
5. Sikring af varig adgang til systemer (post exploitation)

For hvert punkt er der selvfølgelig en hel verden af begreber og teknologier som det desværre ikke er muligt at dække i denne lille opgave.  

Hvis du vil læse mere om it sikkerhed kan denne bog anbefales 
[https://it-sikkerhedsbogen.dk/](https://it-sikkerhedsbogen.dk/)

### Sikring af databaser

Nu hvor du har fået et lille indblik i hvad du risikerer hvis du ikke sikrer dine web applikationer og bagvedliggende systemer har du nok lyst til at vide hvordan du kan sikre dig.  

Det er dog vigtigt at bemærke at du aldrig kan blive 100% sikker fordi der løbende bliver opdaget nye sikkerheds huller og nye teknikker til at bryde eksisterende sikkerhed.  
Sikkerhed er en proces som løbende skal holdes ved lige, også af webudviklere.  

SQL server som vi pt. arbejder med implemeterer sikkerhed på flere niveauer:

- Platformen som serveren kører på (server HW og SW)
- Authentication (bevis for at du er den du udgiver dig for at være)
- Authorization (hvad har applikationen/brugeren lov til at gøre)

**Platform**  
er den fysiske hardware som databasen er installeret på og de netværkssystemer der forbinder til hardware og ultimativt databasen. 

**Authentication**  
handler om at brugere (mennesker og applikationer) skal bevise hvem de er for at få adgang til hhv. database systemet (master databasen: login og DB users) samt de enkelte databaser der kører på serveren.  
SQL server benytter 2 former for authentication, Windows authentication og SQL Server Authentication.  

**Authorization**  
Er styring af hvad den enkelte brugergruppe/bruger/applikation har lov til at gøre i databasen. Dette gælder for eksempel styring af adgang til både databser og databasernes indhold helt ned på kolonne niveau. 

### Roller

Roller beskriver hvem du er og hvad du kan i en given kontekst, du kender sikkert f.eks administrator rollen og bruger rollen.

SQL server anvender 2 sæt roller, server roller og database roller.

Roller skal forstås som en gruppe en given bruger/applikation kan være medlem af.   
Hvis brugeren/applikationen er medlem af rolle gruppen opnås rollens privilegier. 

Oprettelse af brugere og tildeling af roller kan ske grafisk i SSMS eller via SQL kommandoer.

**Server roller**

SQL server har et antal foruddefinerede roller som dækker forskellige niveauer (afhængig af SQL server versionen) af rettigheder til SQL serveren softwaren.

Du kan se et overblik over rollerne i denne artikel [https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/server-level-roles](https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/server-level-roles)  

Rollerne i SQL server 2022 er designet ud fra *Principle of least priviledge(POLP)* som betyder at der kun skal gives adgang til den information og de ressourcer der er nødvendige for at udføre en given opgave.  

Ved at anvende POLP begrænses adgang for en eventuelt angriber, ved at angriberen kun får adgang til et begrænset sæt privilegier, f.eks kun at kunne læse data men ikke slette det.  

POLP er en overordnet sikkerheds strategi, der anvendes overalt hvor der anvendes brugerstyring. 

Du kan læse mere om POLP i SQL server kontekst her  
[https://techcommunity.microsoft.com/t5/azure-sql-blog/security-the-principle-of-least-privilege-polp/ba-p/2067390](https://techcommunity.microsoft.com/t5/azure-sql-blog/security-the-principle-of-least-privilege-polp/ba-p/2067390)  

Det er muligt at lave sine egne server roller, de kaldes *user-defined server level roles*

**Database roller**

Database roller dækker over et antal forudefinerede roller til at styre adgang til de enkelte databaser.
Den enkelte rolle beskriver hvad der gives adgang til. 

For eksempel står der om *db_datawriter:*
```
Members of the db_datawriter fixed database role can add, delete, or change data in all user tables.
```

Læs mere om database roller her:  
[https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/database-level-roles](https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/database-level-roles)

### Eksempler på anvendelse af sikkerhed på SQL server

### Oprettelse af login til MS SQL server

Eksemplet opretter brugeren student01 og tildeler brugeren rollen *db_creator*.
student01 kan således oprette databser på serveren, men har ikke adgang til eksisterende databaser

**SQL**
```SQL
USE master
GO

CREATE LOGIN student01 WITH PASSWORD=N'Aa12341234!'
GO

ALTER SERVER ROLE dbcreator ADD MEMBER student01
GO
```

**Grafisk SSMS**
![images/sec_create_login.png](images/sec_create_login.png)

### Adgang til specifik database

Login giver adgang til serveren, en USER giver adgang til en specifik database.  
Et login kan associeres med mange users. 

```SQL
USE world
GO

CREATE USER student01 FOR LOGIN student01
GO

ALTER ROLE db_datareader ADD MEMBER student01
GO
```

**Grafisk SSMS**
![images/sec_db_access.png](images/sec_db_access.png)

### Adgang til database objekter

Nedenstående fjerner student01 fra db_datareader gruppen og efterfølgende gives adgang til en stored procedure der hedder `spGetCitiesPerCountry`

```SQL
USE [World]
GO

ALTER ROLE [db_datareader] 
DROP MEMBER [student01]
GO

GRANT EXECUTE ON 
[dbo].[spGetCitiesPerCountry] TO [student01]
GO

```

**Grafisk SSMS**  
![images/sec_sp_access.png](images/sec_sp_access.png)

## Instruktioner

Nu er det din tur til at lege med datase sikkerhed :-)

1. Lav et nyt login til din SQL Server, dette login skal ikke have rettigheder til at gøre noget på din server
2. Opret en bruger til din World databasen, brugeren skal kun have rettigheder til at eksekvere stored procedures (du vælger hvilke)
3. Test rettighederne ved at lave en ny forbindelse til serveren, afprøv forskellige forespørgsler og verificer at brugeren ikke kan andet end at eksekvere stored procedures

## Links

- 11 Steps to Secure SQL in 2022 [https://www.upguard.com/blog/11-steps-to-secure-sql](https://www.upguard.com/blog/11-steps-to-secure-sql)
- Microsoft SQl security dokumentation [https://learn.microsoft.com/en-us/sql/relational-databases/security](https://learn.microsoft.com/en-us/sql/relational-databases/security)