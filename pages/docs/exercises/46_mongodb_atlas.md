---
hide:
  - footer
---

# Opgave 46 - MongoDB Atlas

## Information

MongoDB Atlas er mongodb's cloud tjeneste (database as a service - DAAS). Her kan du hoste mongodb databaser og det er gratis for små databaser.  
Formålet med at hoste din todo database i skyen er, udover at lære om det, at node API applikationen senere skal hostes eksternt på heroku og derfor er det nødvendigt at databasen kan tilgås fra heroku. 

Det er ret nemt at oprette en bruger og database på MongoDB Atlas og det er det du skal gøre i denne opgaves **Del 1**

Du skal bruge den dokumentation der er tilgængelig, det vil sige at der er ikke præcise instruktioner i del 1 af opgaven, du skal selv finde læsningen ved hjælp af dokumentation.  
Du er selvfølgelig altid velkommen til at spørge efter hjælp fra enten din underviser eller dine medstuderende :-)

I **Del 2** skal du bruge npm pakken `dotenv` til at beskytte din mongodb connection string så den ikke senere bliver eksponeret via GIT.  
Formålet er at lære hvordan "hemmeligheder" håndteres i applikationer så de ikke deles uhensigtsmæssige steder.   

## Instruktioner

Net ninja har lavet en video om hvordan du opretter dig på MongoDB Atlas [https://youtu.be/084rmLU1UgA](https://youtu.be/084rmLU1UgA)  
Der er også en ret god dokumentation på MongoDB atlas [https://www.mongodb.com/docs/atlas/getting-started/](https://www.mongodb.com/docs/atlas/getting-started/)

**Del 1 - opret og forbind database på MongoDB Atlas**

1. Opret en konto på MongoDB ATlas
2. Lav en database bruger og en database
3. Erstat connection string i node todo applikationen (`db.js`) med den fra atlas + bruger + password + cluster + database
Eksempel på hvor de forskellige ting skal skrives in i connection string er herunder    
```js
const uri = "mongodb+srv://<username>:<password>@<cluster>.1qumu.mongodb.net/<database>?retryWrites=true&w=majority";
```
4. Test at din node applikation kan forbinde til atlas db databasen og test alle endpoints med postman og atlas databasen

**Del 2**

Dokumentation for `dotenv` er her [https://github.com/motdotla/dotenv#readme](https://github.com/motdotla/dotenv#readme)

1. Installer dotenv i din node todo applikation `npm install dotenv --save` 
2. lav fil i roden af projektet der hedder `.env` og indsæt din connection string på denne måde `ATLAS_CON_STRING="Din connection string her"`
3. I `db.js` skal du importere dot env på første linje med `require('dotenv').config();`
4. I `db.js` erstatter du nu uri variablen med `const uri = process.env.ATLAS_CON_STRING`
5. Test at din node applikation kan forbinde til atlas db databasen

## Links
