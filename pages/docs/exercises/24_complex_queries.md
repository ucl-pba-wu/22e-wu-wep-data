---
hide:
  - footer
---

# Opgave 24 - Komplekse forespørgsler

## Information

Dette er en opgave der bygger videre på **Opgave 23 - Bike stores database** og giver mulighed for at træne forespørgsler hvor både `JOIN`, `GROUP BY` og SQL funktioner indgår.

Forespørgslerne skal udføre på `BikeStores` databasen

![images/bike_stores_schema.png](images/bike_stores_schema.png)

## Instruktioner

Lav følgende forespørgsler:

- Hvad er prisen pådet dyreste produkt ? Rund op til nærmeste heltal.
- Vis kundernes firstname, lastname og initialer. F.eks. Deborah Burks (DB)
- Vis antal produkter pr. brand_id. Vis kun brands der har mere end 100 produkter
- Vis antal produkter pr. brand_name og antal produkter pr. category id. Vis kun kategorier der har mere end 50 produkter
- Hvor mange ordrer er oprettet af den ansatte der hedder Mireya Copeland ? 
- Vis alle butiksnavne og hvor mange produkter de hver har på lager
- Vis alle i produkter i kategorien 'Comfort Bicycles'. Vis kun produkt navn, sorter alfabetisk.

## Links

