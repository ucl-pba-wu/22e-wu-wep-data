---
hide:
  - footer
---

# Opgave 1 - Fagets læringsmål

## Information

I denne opgave skal i undersøge fagets læringsmål.

Formålet er at sikre at i ved:

- hvor læringsmålene kan findes
- hvad de betyder
- hvad i kan bruge dem til

og ikke mindst, bliver det lettere at huske læringsmålene hvis i har arbejdet lidt med dem.

## Instruktioner

1. Find læringsmålene og del linket mellem jer
2. Brug "tænk, par, del" til at finde de læringsmål i forestiller jer er relevante for database faget og hvad i kan bruge dem til, begrund jeres valg og noter dem.
4. Vi afslutter med "30 til buffet"

## Links

Alle ressourcer kan findes i dagens plan på itslearning
