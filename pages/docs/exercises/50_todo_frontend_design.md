---
hide:
  - footer
---

# Opgave 50 - API frontend - design

## Information

I de sidste par uger har i udviklet en todo API i node.js der bruger en mongodb database.  
Formålet med denne opgave er at designe, med wireframes, hvordan en simpel frontend der konsumerer jeres API, altså kan hente, gemme, ændre, slette og vise data fra jeres todo API.

Denne opgave er rent design, altså handler det om at identificere hvilke elementer der skal være i frontenden for at i kan interagere med de forskellige API endpoints.  

I skal samarbejde i jeres gruppe og det er vigtigt at i fokuserer på funktionalitet fremfor design.

Til inspiration kan er her et screenshot af hvordan jeg har valgt at opbygge min frontend

![images/todo_frontend.png](images/todo_frontend.png)

## Instruktioner

1. Design med wireframes hvordan frontende skal se ud. Frontenden skal indeholde:
    1. Liste med pagination der kan vise alle todo items fra databasen. Pagination skal indeholde 10 elementer pr. page.
    2. Form til at oprette nye todo items
    3. Form til at ændre eksisterende todo items
    4. Input til at søge efter todo items

## Links

- diagrams.net [https://app.diagrams.net/](https://app.diagrams.net/)