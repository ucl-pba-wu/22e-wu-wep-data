---
hide:
  - footer
---

# Opgave 6 - Pet hospital

## Information

I denne opgave skal i ud fra data i et excel ark lave et logisk ER diagram/data model.

Data kommer fra et dyre hospital og jeres opgave er at designe en database der kan erstatte hospitalets excel ark.

Det vi har fået fra hospitalet kan ses her:

![images/pet_hosp_report.png](images/pet_hosp_report.png)

## Instruktioner

1. Analyser databehov som beskrevet i opgave 2 og notér hvad i har brug for i databasen. Eksempel:
    - A pet has one owner
    - An owner can have many pets, a pet has a certain type....
2. Lav et logisk ER diagram som beskrevet i opgave 3, brug [https://app.diagrams.net/](https://app.diagrams.net/)
3. Del et link til jeres diagram i dette [regneark](https://docs.google.com/spreadsheets/d/1vNcc9raJnRWiHLGhhU_C0RNVjKTsCaIPEHTh9i25Ie4/edit?usp=sharing), husk at bruge det rigtige gruppe nummer og angiv jeres navne.


## Links
