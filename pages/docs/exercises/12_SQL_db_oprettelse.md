---
hide:
  - footer
---

# Opgave 12 - Opret Database med SQL

## Information

SQL bruges også til at oprette databaser, ændre databaser og indsætte data i databaser. 
Du får også en gennemgang og forklaring af Primary Key (PK) og Foreign Key (FK) 
Denne øvelse viser de vigtigeste SQL kommandoer til at oprette og ændre, samt indsætte data i databaser.  
Du skal også lave et par forespørgsler i databasen for at bekræfte at det virker.

Artiklerne i instruktionerne bruger SSMS men det samme kan udføres fra *Query* vinduet i Azure Data Studio.

Det er vigtigt at du ikke bare læser artiklerne, men udfører eksemplerne der vises for at opnå læring.  
Når du er færdig med opgaven skal du have en database som hedder ***our_first_database*** med tilhørende data.
  
![images/our_first_database_schema.png](images/our_first_database_schema.png)  
*ER Diagram*  


![images/our_first_database_tables.png](images/our_first_database_tables.png)  
*Tabeller med tilhørende data*  


## Instruktioner

1. Opret din første database ***our_first_database*** og tilhørende tabeller ved at følge denne artikel  
[https://www.sqlshack.com/learn-sql-create-database-create-table/#ToC](https://www.sqlshack.com/learn-sql-create-database-create-table/#ToC)
2. Indsæt data i ***our_first_database*** databasens tabeller ved at følge denne artikel  
[https://www.sqlshack.com/learn-sql-insert-into-table/](https://www.sqlshack.com/learn-sql-insert-into-table/)
3. Læs om Primary Key (PK)  
[https://www.sqlshack.com/learn-sql-primary-key/](https://www.sqlshack.com/learn-sql-primary-key/)
4. Læs om Foreign key (FK)  
[https://www.sqlshack.com/learn-sql-foreign-key/](https://www.sqlshack.com/learn-sql-foreign-key/)
5. Genbesøg SELECT og lav forespørgsler i ***our_first_database***  
[https://www.sqlshack.com/learn-sql-select-statement/](https://www.sqlshack.com/learn-sql-select-statement/)

## Links
