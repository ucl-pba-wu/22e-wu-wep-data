---
hide:
  - footer
---

# Opgave 27 - SQL indexes

## Information

Når en databsen indeholder rigtig meget data vil forespørgsler kunne tage lang tid at eksekvere.  
Eksekveringstiden afhænger, udover størrelsen på databasen, af serverens performance (harddisk og processor) samt forespørgslens kompleksitet.

En forespørgsel tager måske et par sekunder hvilket i nogel tilfælde kan være et nogenlunde acceptabelt resltat, men til web applikationer er et par sekunder meget lang tid. 
Hvis der er flere forespørgsler, måske flere hundrede på en gang bliver ventetiden bare endnu dårligere. 

Ofte er det ikke alt i ene databse der bliver forespurgt ofte, hvilke data i databsen der er mest forespurgt afhænger af applikationen.

Du kan sammenligne et indeks med stikordsregistret i en bog, har kan du hurtigt slå et ord op og se hvilke sider i bogen der omtaler det specifikke ord.

Jeg har prøvet at researche for at sætte en opgave op, som i kan bruge til at se hvordan et indeks i en database med en tabel der har flere millioner linjer kan optimeres med et indeks, men jeg kunne ikke få et ordentligt resultat ud af det.
Derfor er denne opgave mest en teori opgave så i kan få viden om indeksering i relationelle databaser, på den måde kan i bruge det hvis i senere i jeres karriere støder på et database problem, der kan løses med at lave et eller flere indeks på meget brugte kolonner.  

Under min research stødte jeg på en god artikel der viser hvordan man kan generere en masse test data. Det kan i prøve hvis i synes det er spændende.
Linket til artiklen er her [https://www.sqlshack.com/how-to-generate-random-sql-server-test-data-using-t-sql/](https://www.sqlshack.com/how-to-generate-random-sql-server-test-data-using-t-sql/)  
Artiklen linker til en database der er fundamentet til generering af data, linket i artiklen virker ikke så her er et link der virker [https://learn.microsoft.com/en-us/sql/samples/adventureworks-install-configure](https://learn.microsoft.com/en-us/sql/samples/adventureworks-install-configure) 

I skal også kende til begrebet *execution plan* som er den måde sql databasen beregner den bedste afvikling af en given forespørgsel.  
Execution plan kan give jer en ide om hvor hurtigt/effektivt jeres forespørgsel bliver udført og er dermed et værktøj til at analysere en forespørgsels performance.

## Instruktioner

1. Hvis du endnu ikke har set Socratica videoen om indexes så bør du gøre det nu [https://youtu.be/fsG1XaZEa78](https://youtu.be/fsG1XaZEa78)
2. Læs denne artikel om indexes og hvordan de oprettes og slettes [https://www.tutorialspoint.com/t_sql/t_sql_indexes.htm](https://www.tutorialspoint.com/t_sql/t_sql_indexes.htm)
3. Læs om execution plan [https://www.sqlshack.com/execution-plans-in-sql-server/](https://www.sqlshack.com/execution-plans-in-sql-server/)
4. Prøv at kigge på execution plan for nogle af dine forespørgsler, læg mærke til hvilke dele af forespørgslerne der bruger flest ressourcer og hvordan ressourcer måles (cpu etc.) i hhv. Query Plan, Plan Tree og Top Operations
5. Udvælg en forespørgsel du har lavet tidligere, hvilke kolonner i hvilke tabeller bruger forespørgslen ? 
6. Kør forespørgslen og kig i execution plan hvordan den performer
7. Lav et indeks på en kolonne der bliver brugt i forespørgslen
8. Kør forespørgslen og kig i execution plan om performance er blevet bedre

## Links

- Microsoft execution plan dokumentation [https://learn.microsoft.com/en-us/sql/relational-databases/performance/execution-plans](https://learn.microsoft.com/en-us/sql/relational-databases/performance/execution-plans)
