---
hide:
  - footer
---

# Opgave 36 - MongoDB CRUD

## Information

CRUD står for Create Read Update Delete og er de typiske opreationer du udfører når du interagerer med en database.   

Begrebet er ikke begrænset til MongoDB men dækker både databaser og applikationer, det er dermed en fællesbetegnelse og et fagbegreb der generelt bruges.

Artiklen her tager udgangspunkt i CRUD opreationer i MongoDB.

**Create** opretter ting i databasen
**Read** Henter data fra databasen
**Update** Opdaterer ting i databasen
**Delete** Sletter ting i databasen
  
### Create

I MongoDB kan et dokument indsættes med syntaksen `db.collection.insertOne()`

Flere dokumenter indsættes med `db.collection.insertMany()`

#### insertOne()

Eksemplet herunder viser brug af insertOne i users collection. Kommandoen indsætter et dokument med 3 `key:value` par.

```js linenums="1" title="insertOne"
db.users.insertOne(
  {
    name: "sue",
    age: 26,
    status: "pending"
  }
)
```  

I `SQL` svarer det til at indsætte en række i en tabel med syntaksen

```SQL linenums="1" title="SQL INSERT"
INSERT INTO users (name, age, status)
VALUES ('sue', 26, 'pending')
```

#### insertMany()

insertMany fungerer på samme måde som insertOne men her kan flere dokumenter indsættes i den samme collection.  
Dog er det vigtigt at angive flere dokumenter i et array indeholdende de JSON objekter der skal indsættes.

```js linenums="1" title="insertMany"
db.users2.insertMany(
  [
    {
      name: "sue",
      age: 26,
      status: "pending"
    },
    {
      name: "joe",
      age: 21,
      status: "active"
    },
    {
      name: "bobby",
      age: 42,
      status: "pending"
    }
  ]
)
```

### Read

Læsning af data fra databasen gøres med `db.collection.find()`

#### find()

Med find kommandoen kan dokumenter hentes fra en collection.

Den generiske og fulde syntaks for find er

```js linenums="1" title="generisk find syntaks"
db.collection.find( <query filter>, <projection> )
```

en find uden filtrering, som eksemplet herunder viser, returnerer alt fra user collection. 

```js linenums="1" title="db.users.find resultat"
test> db.users.find()
[
  {
    _id: ObjectId("634817d064272c98a3e70e56"),
    name: 'sue',
    age: 26,
    status: 'pending'
  }
]
test> db.users.find()
[
  {
    _id: ObjectId("634817d064272c98a3e70e56"),
    name: 'sue',
    age: 26,
    status: 'pending'
  },
  {
    _id: ObjectId("634a8fd4b4ec36a571e1a3d8"),
    name: 'joe',
    age: 21,
    status: 'active'
  },
  {
    _id: ObjectId("634a901bb4ec36a571e1a3d9"),
    name: 'bobby',
    age: 42,
    status: 'pending'
  }
]
```

Med filtre er det muligt at lave mere præcise søgninger i en collection.

Eksemplet herunder bruger et par argumenter til find() kommandoen.

- `{ age: { $gt: 21}}` er kriteriet for søgningen, her at age skal være over 21
- `{ _id: 0, name: 1, age: 1}` kaldes projection og specificerer hvilke `key:value` felter der skal medtages i resultatet. Her udelades _id og name samt age medtages.
- `.limit(5)` modificerer den cursor som returneres, her til at give 5 resultater. Læs mere om cursor metoder her [https://docs.mongodb.com/manual/reference/method/js-cursor/index.html](https://docs.mongodb.com/manual/reference/method/js-cursor/index.html)

```js linenums="1" title="db.users.find filter"
db.users.find(            //collection
  { age: { $gt: 21}},     //query criteria
  { _id: 0, name: 1, age: 1}  //projection
).limit(5)                //cursor modifier
```

Resultatet af db.users.find filter kommandoen kan ses herunder.

```js linenums="1" title="db.users.find filter resultat"
[ 
  { name: 'sue', age: 26 }, 
  { name: 'bobby', age: 42 } 
]
```

### Update og Delete

Nu hvor du har set eksempler på hvordan create og find fungerer vil det være let for dig selv at undersøge hvordan man opdaterer og sletter.  
Du kan bruge MongoDB dokumentationen til at læse om metoderne og deres syntaks.

#### UPDATE

- updateOne [https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/](https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/)
- updateMany [https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/](https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/)
- replaceOne [https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/](https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/)

#### DELETE

- deleteOne [https://docs.mongodb.com/manual/reference/method/db.collection.deleteOne/#db.collection.deleteOne](https://docs.mongodb.com/manual/reference/method/db.collection.deleteOne/#db.collection.deleteOne)
- deleteMany [https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/#db.collection.deleteMany](https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/#db.collection.deleteMany)

## Instruktioner

1. Opret en ny MongoDB database, kald den *classiccars*
2. Lav en collection der hedder clients til nedenstående data ![images/mongo_classic_cars.png](images/mongo_classic_cars.png)
3. Udfør følgende forespørgsler
    1. Find alle biler for personer med fornavnet "Gaston"
    2. Vis den by Paul Miller bor i
    3. Find alle personer med et efternavn der starter med "B"
    4. Find alle der ejer en "Renault"
    5. Find alle personer der bor i enten Zurich eller Rome

## Links

### MongoDB CREATE og READ dokumentation

#### CREATE
- insertOne [https://docs.mongodb.com/manual/reference/method/db.collection.insertOne/](https://docs.mongodb.com/manual/reference/method/db.collection.insertOne/)
- insertMany [https://docs.mongodb.com/manual/reference/method/db.collection.insertMany/](https://docs.mongodb.com/manual/reference/method/db.collection.insertMany/)

#### READ

- find [https://docs.mongodb.com/manual/reference/method/db.collection.find/](https://docs.mongodb.com/manual/reference/method/db.collection.find/)

