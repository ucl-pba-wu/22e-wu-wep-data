---
hide:
  - footer
---

# Opgave 31 - SQL angreb

## Information

Aplikationer der anvender SQL databaser er sårbare overfor det der hedder *SQL injections*  
SQL injections er mulige når et input felt i f.eks en web applikation tillader at eksekvere SQL kode.  

Den bedste måde at forstå hvad der sker når et input ikke er beskyttet, er at se det blive udført.
I forberedelsen til denne opgave har jeg linket til en video der forklarer og viser hvordan et simpelt SQL injection angreb udføres.

NetworkChuck - SQL Injections are scary!!  
[https://youtu.be/2OPVViV-GQk](https://youtu.be/2OPVViV-GQk)

Der er også en ok artikel på w3schools som forklarer hvordan man kan manipulere et input felt til at eksekvere uønsket SQL kode i databasen.
[https://www.w3schools.com/sql/sql_injection.asp](https://www.w3schools.com/sql/sql_injection.asp)

Web udviklere bør kende til Open Web Application Security Project(OWASP) [https://owasp.org/](https://owasp.org/).    
OWASP er en nonprofit organisation der har til formål at hjælpe udviklere med at sikre webapplikationer.  

OWASP udgiver årligt en top 10 over de mest kritiske sårbarheder i webapplikationer
[https://owasp.org/www-project-top-ten/](https://owasp.org/www-project-top-ten/)

På 3. pladsen ligger injections [https://owasp.org/Top10/A03_2021-Injection/](https://owasp.org/Top10/A03_2021-Injection/)

### Hvordan kan injections forhindres

**Sanitering**  
I både frontend og backend bør bruger input renses for karakterer der ikke er relevante.  
Det kan gøres ved for eksempel at checke at det der indtastes er en valid email adresse, at et password felt ikke er mere en x antal karakterer, lister med forbudte karakterer osv.  

Frontend validering kan nemt omgås ved at slå javascript fra i browseren eller ved at køre en POST request med f.eks [curl](https://curl.se/) (der findes mange andre metoder)  
Derfor vil jeg påstå at validering i frontend primært skal gøres for at hjælpe brugeren med beskeder når noget er indtastet forkert og sikre at der ikke sendes forkerte forespørgsler fra GUI.  
MDN har skrevet en artikel om client side validation [https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation](https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation)

**Stored Procedures**

Brug stored procedures! 
Her kan du begrænse adgang så appilkationen kun kan køre en stored procedure og ikke gøre andre ting i databasen. 
Samtidig kan du sikre at de parametre der anvendes i proceduren er den rigtige datatype og længde

```SQL
CREATE PROCEDURE VerifyUser 
	@username varchar(50), 
	@password varchar(50) 
AS 
BEGIN 
	SELECT * FROM UserTable  
	WHERE UserName = @username  
	AND Password = @password; 
END 
GO
```

**SQL Parametre**

Brug SQL parametre i din backend. Parametre checkes i forhold til tabellens kolonne og fjerner muligheden for at manipulere SQL kommandoen.

eksempel i ASP.NET Razor (C#)
```c#
txtUserId = getRequestString("UserId");
txtSQL = "SELECT * FROM Users WHERE UserId = @0";
db.Execute(txtSQL,txtUserId);
```

## Instruktioner

0. Se [networkchuck videoen](https://youtu.be/2OPVViV-GQk) eller læs [W3Schools artiklen](https://www.w3schools.com/sql/sql_injection.asp)
1. Lav en konto på tryhackme [https://tryhackme.com/signup](https://tryhackme.com/signup)
2. Følg instruktionerne i SQL injection rummet og fuldfør opgaverne [https://tryhackme.com/room/sqlinjectionlm](https://tryhackme.com/room/sqlinjectionlm)
3. Læs om de forskellige web applikation sårbarheder i OWASP top 10 [[https://owasp.org/www-project-top-ten/](https://owasp.org/www-project-top-ten/)]([https://owasp.org/www-project-top-ten/](https://owasp.org/www-project-top-ten/))

## Links

- Microsoft artikel om SQL injections  
[https://learn.microsoft.com/en-us/sql/relational-databases/security/sql-injection](https://learn.microsoft.com/en-us/sql/relational-databases/security/sql-injection)