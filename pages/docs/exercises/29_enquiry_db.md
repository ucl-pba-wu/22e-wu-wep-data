---
hide:
  - footer
---

# Opgave 29 - Enquiry DB

## Information

Denne opgave handler om at modellere og oprette en databse fra start til slut. Altså fra kundens oplæg til ER diagram til oprettet og funktionel database.

### Case oplæg

Jeres kunde (en virksomhed) vil gerne have en kontakt formular på deres hjemmeside. Formularen skal kunne håndtere information om en kunde der opretter formularen, samt hvilken type forespørgsel personen efterspørger.  
Data som afsendes fra formularen skal kunne håndtere det som vises i eksemplet herunder:

| Label | Data example |
| --- | --- |
| Firstname | Jane |
| Lastname | Doe |
| Email | jane@doe.dk |
| Enquiry type | e.g. Product enquiry, General Info, Support etc.|
| Preferred case worker | List of employee names the customer can choose from, but not required to |
| Message | .... |

Virksomheden har også brug for et interface som kan bruges af en medarbejder til at overvåge om der komme henvendelser fra kunder via formularen.  
Når en henvendelse ankommer vil medarbejderen sende den videre til den person i virksomheden der skal behandle henvendeslen, det foregår vi email.  
Medarbejderen skal også kunne opdatere "preferred case worker" kolonnen hvis det er nødvendigt. 
Medarbejderen skal også kunne markere henvendelsen som fuldført.

En mockup af den liste medabejderen kigger på ser sådan her ud:

![images/enquiry_db_list.png](images/enquiry_db_list.png)

## Instruktioner

Lav følgende:

1. Wireframes (skal bare gøres så i selv forstår dem, må gerne tegnes i hånden)
    1. Kontakt formular
    2. Interface til medarbejder
2. Fysisk ER diagram (husk passende datatyper)
3. `.sql` fil med SQL `CREATE` kommandoer der opretter database og tabeller
4. `.sql` fil med SQL `INSERT` kommandoer der indsætter data i databasen
5. `.sql` fil med SQL `SELECT` kommandoer der viser data til brug i medarbejder interfacet
6. `.sql` fil med `UPDATE` kommando(er) der kan ændre status på en henvendelse (det som medarbejderen gør)

### Generelt

Brug views, stored procedures, indexes og transaktioner hvor i vurderer det giver mening.

Hvis i synes noget i instruktionerne er uklart så er det ok, at lave antagelser om hvordan det skal være.

## Links
