---
hide:
  - footer
---

# Opgave 39 - MongoDB indexes

## Information

I **Opgave 27 - SQL indexes** lærte i om indeksering i en database.  

Kort fortalt kan et indeks sammenlignes med stikordsregistret i en bog, har kan du hurtigt slå et ord op og se hvilke sider i bogen der omtaler det specifikke ord.

Indeksering er relevant når en forespørgsel skal afvikles i en database med tusindvis af dokumenter, her kan det tage længere tid at finde data hvilket kan optimeres ved at lave et indeks.  

Et indeks laves på et enkelt felt, f.eks *runtime* feltet i hbo kollektionen eller *cuisine* feltet i restaurants kollektionen.  
De nævnte eksempler er meget små databaser så her skal du opfatte det som et middel til at lære hvordan du opretter indexes og måler performance før og efter oprettelse.

Du kan bruge explain kommandoen for at få vist statistik på en forespørgsel.

## Instruktioner

1. Se net ninja videoen om indeksering - link er herunder.  
2. Læs MongoDB dokumentationen om indeksering for at få et overblik
3. 1. Hent filen: [https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/hbo.js](https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/hbo.js) du kan højreklikke på den i din browser og vælge *gem som*
3. Mål performance på denne forespørgsel `db.hbo.find({runtime: 60})` brug `.explain('executionStats')` eksempel:  
```js
db.hbo.find({runtime: 60}).explain('executionStats')
```
4. Noter antallet af dokumenter fra `totalDocsExamined` feltet og hvor mange resultater der bliver returneret fra `nReturned`.  
I mit tilfælde er det:  
```js
executionStages: {
      stage: 'COLLSCAN',
      filter: { runtime: { '$eq': 60 } },
      nReturned: 12,
      executionTimeMillisEstimate: 0,
      works: 22,
      advanced: 12,
      needTime: 9,
      needYield: 0,
      saveState: 0,
      restoreState: 0,
      isEOF: 1,
      direction: 'forward',
      docsExamined: 20
```
5. Opret et indeks på forespørgslen med `db.hbo.createIndex({runtime: 60})`
6. Bekræft at indekset er oprettet med `db.hbo.getIndexes()`
```js
movies> db.hbo.getIndexes()
[
  { v: 2, key: { _id: 1 }, name: '_id_' },
  { v: 2, key: { runtime: 60 }, name: 'runtime_60' }
]
```
7. Mål nu performance igen, på samme måde som i trin 4 og sammenlign resultater.  
```js
executionStages: {
      stage: 'FETCH',
      nReturned: 12,
      executionTimeMillisEstimate: 11,
      works: 13,
      advanced: 12,
      needTime: 0,
      needYield: 0,
      saveState: 1,
      restoreState: 1,
      isEOF: 1,
      docsExamined: 12,
```
8. Slet dit index med `db.hbo.dropIndex({ runtime: 60 })` og bekræft sletning med `db.hbo.getIndexes()`

## Links

- The net ninja - Indexes [https://youtu.be/D14wWW9EEx8](https://youtu.be/D14wWW9EEx8)
- MongoDB indexing dokumentation [https://www.mongodb.com/docs/manual/indexes/](https://www.mongodb.com/docs/manual/indexes/)