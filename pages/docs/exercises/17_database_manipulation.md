---
hide:
  - footer
---

# Opgave 17 - Database manipulation

## Information

Efter oprettelse af en database er det nyttigt at kunne ændre i den.  
I denne opgave skal du studere og anvende SQL kommandoerne:

- UPDATE
- DELETE
- TRUNCATE
- DROP

## Instruktioner

1. Læs om UPDATE kommandoen i microsoft SQL dokumentationen [https://docs.microsoft.com/en-us/sql/t-sql/queries/update-transact-sql?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/t-sql/queries/update-transact-sql?view=sql-server-ver15)
2. Brug UPDATE på en tabel i en af dine databaser
3. Læs om DELETE kommandoen i microsoft SQL dokumentationen [https://docs.microsoft.com/en-us/sql/t-sql/statements/delete-transact-sql?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/t-sql/statements/delete-transact-sql?view=sql-server-ver15)  
**ADVARSEL - Hvis du bruger DELETE uden WHERE så slettes alt indhold i tabellen!**
4. Brug DELETE på en tabel i en af dine databaser **HUSK WHERE**
5. Læs om TRUNCATE kommandoen i microsoft SQL dokumentationen [https://docs.microsoft.com/en-us/sql/t-sql/statements/truncate-table-transact-sql?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/t-sql/statements/truncate-table-transact-sql?view=sql-server-ver15)
6. Brug TRUNCATE på en tabel i en af dine databaser
7. Læs om DROP TABLE kommandoen i microsoft SQL dokumentationen [https://docs.microsoft.com/en-us/sql/t-sql/statements/drop-table-transact-sql?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/t-sql/statements/drop-table-transact-sql?view=sql-server-ver15)
8. Implementer DROP TABLE i starten af en af dine SQL Scripts så alle tabeller droppes inden de oprettes på ny. På den måde sikrer du at databasen er tom inden du opretter den igen.
9. Læs om DROP DATABASE kommandoen i microsoft SQL dokumentationen [https://docs.microsoft.com/en-us/sql/t-sql/statements/drop-database-transact-sql?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/t-sql/statements/drop-database-transact-sql?view=sql-server-ver15)
8. Implementer DROP DATABASE i starten af en af dine SQL Scripts så databasen slettes inden den oprettes på ny, du skal sørge for at du i dit script checker om databasen eksisterer, så den kun droppes hvis den eksisterer

## Links

På ITSLearning er der som forberedelse linket til to videoer der forklarer UPDATE og DELETE.
