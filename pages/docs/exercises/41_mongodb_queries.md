---
hide:
  - footer
---

# Opgave 41 - MongoDB Queries

## Information

Formålet med denne opgave er at træne mongoDB forespørgsler.  
Husk at bruge mongoDB dokumentationen og også søge på nettet efter hjælp.  
Det er selvfølgelig også en god ide at spørge hinanden i klassen :-)

## Instruktioner

Alle kommandoer skal gemmes i en fil der hedder `hbo_solution.js`

1. Hent filen: [https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/hbo.js](https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/hbo.js) du kan højreklikke på den i din browser og vælge *gem som*
2. Opret hbo databasen i mongodb med mongosh
3. Løs følgende delopgaver ved brug af aggregation pipeline i mongosh, hver delopgave har angivet korrekt output, som du kan bruge til at kontrollere dit output
    1. Vis air date og air time på episoden ved navn "Winter is coming"  
    *Korrekt output*
    ```js
    [ { _id: 4952, airdate: '2011-04-17', airtime: '21:00' } ]
    ```
    2. Vis navnet på episode 5, sæson 2. Vis kun navnet.  
    *Korrekt output*
    ```js
    [ { _id: 4966, name: 'The Ghost of Harrenhal' } ]
    ```
    3. Vis id for det dokument hvor feltet "original image" indeholder strengen '2669.jpg'​  
    *Korrekt output*
    ```js
    [ { _id: 4953 } ]
    ```
    4. Vis navne på alle episoder der har en runtime mellem 50 og 60 minutter (begge tal inklusiv). Sorter dem faldende efter navn.  ​
    *Korrekt output*  
    ```js
    [
      { _id: 4952, name: 'Winter is Coming' },
      { _id: 4964, name: 'What is Dead May Never Die' },
      { _id: 4971, name: 'Valar Morghulis' },
      { _id: 4969, name: 'The Prince of Winterfell' },
      { _id: 4959, name: 'The Pointy End' },
      { _id: 4967, name: 'The Old Gods and the New' },
      { _id: 4963, name: 'The Night Lands' },
      { _id: 4953, name: 'The Kingsroad' },
      { _id: 4966, name: 'The Ghost of Harrenhal' },
      { _id: 4965, name: 'Garden of Bones' },
      { _id: 4955, name: 'Cripples, Bastards, and Broken Things' },
      { _id: 4970, name: 'Blackwater' },
      { _id: 4960, name: 'Baelor' },
      { _id: 4968, name: 'A Man Without Honor' },
      { _id: 4957, name: 'A Golden Crown' }
    ]
    ```
    5. Vis hvor mange episoder der er pr. sæson, grupper og sorter efter sæson stigende.  
    *Korrekt output*
    ```js
    [ { _id: 2, episodes: 10 }, { _id: 1, episodes: 10 } ]
    ```
    6. Vis gennemsnits runtime af episoderne pr. sæson   
    *Korrekt output*
    ```js
    [ { _id: 1, avg: 61.5 }, { _id: 2, avg: 59.5 } ]
    ```

## Links
