---
hide:
  - footer
---

# Opgave 3 - Den relationelle datamodel

## Information

I denne opgave skal i tilegne jer viden om den relationelle data model og centrale begreber knyttet til modellen.

Opgaven er en gruppe opgave og i skal samlet notere jeres svar så hele gruppen har tilgang til svarene.

## Instruktioner

1. Hvert gruppe medlem læser denne artikel om den relationelle data model:  
   [https://www.digitalocean.com/community/tutorials/understanding-relational-databases](https://www.digitalocean.com/community/tutorials/understanding-relational-databases)

2. I gruppen besvarer i følgende spørgsmål:
    1. Hvilke elementer indeholder en _tabel_ ?
    2. Hvad er en _Primary Key_ ?
    3. Hvad er en _Foreign key_ ?
    4. Findes der andre navne for _Row_ ?
    5. Findes der andre navne for _Column_ ?
    6. Giv et bud på hvilke elementer, der har relationer i en relationel database ?

## Links
