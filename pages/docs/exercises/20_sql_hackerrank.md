---
hide:
  - footer
---

# Opgave 20 - Træn SQL forespørgsler på hackerrank

## Information

Opgaven her er ikke obligatorisk men en mulighed for at du kan træne JOIN og forespørgsler.

## Instruktioner

1. Lav en konto på [https://www.hackerrank.com/](https://www.hackerrank.com/)
2. Vælg SQL
3. Vælg MS SQL som sprog i editoren
4. Udfør forespørgslerne

![images/hackerrank.png](images/hackerrank.png)