---
hide:
  - footer
---

# Opgave 23 - Bike stores database

## Information

I denne opgave skal du installere en database der hedder bike stores og lave forespørgsler for at træne det du har lært i opgave **Opgave 21 - Funktioner i SQL** og **Opgave 22 - Gruppering af data**

Databasens skema er angivet i nedenstående ER diagram

![images/bike_stores_schema.png](images/bike_stores_schema.png)

## Instruktioner

1. download de 2 filer tilhørende BikeStores fra ITSLearning
2. Installer databasen og tabeller med den første `.sql` fil
3. Fyld data i dabasen med den anden `.sql` fil
4. Lav følgende forespørgsler i `BikeStores` databasen
    - Hvor mange kunder er der ?
    - Vis alle ordrer hvor shipped date er over 2 dage senere end required date
    - Hvad er gennemsnits list price for butikkens produkter, vis resultat som heltal
    - Hvor mange kunder er der pr. state ?
    - Vis antal produkter grupperet efter model year
    - Vis brand name antal produkter pr. brand. Vis brand med flest produkter først.
    - Vis totalpris pr. ordre   

## Links

- [https://www.sqlservertutorial.net/sql-server-sample-database/](https://www.sqlservertutorial.net/sql-server-sample-database/)