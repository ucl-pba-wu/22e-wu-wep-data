---
hide:
  - footer
---

# Opgave 5 - Data modellering med ER Diagram

## Information

I denne opgave skal i tegne logiske Entity Relation (ER) Diagrammer.  
Målet er at blive fortrolig med brugen af logiske ER Diagrammer.

Opgaven er en gruppe opgave og i skal samarbejde om opgaven.  
Brug [https://app.diagrams.net/](https://app.diagrams.net/) og gem jeres løsninger så alle i gruppen kan tilgå dem.

ER diagrammer er et design værktøj og en visuel repræsentation af en data models enheder, enhedens egenskaber og relationen mellem enheder.  
Jeg bruger ordet enheder her som er datamodel terminologi, men i daglig tale er det mere normalt at omtale det som tabeller og data modellen omtales ofte som databasen.  

Der findes 3 stadier af et ER diagram, et konseptuelt, et logisk og et fysisk.

![images/er_diagram_types.png](images/er_diagram_types.png)

- Det konseptuelle ER diagram (konseptuel data model) er et overblik over hvad som skal inkluderes i databasen.
- Det logiske ER diagram (logisk data model) er mere detaljeret og indeholder enhedens egenskaber samt primary key (PK) og foreign key (FK)
- Det fysiske ER diagram (fysisk datamodel) tilføjer tabel navne, kolonne navne og data typer.

Læs mere om de forskellige data modeller her:  
[https://www.vertabelo.com/blog/crow-s-foot-notation-in-vertabelo/](https://www.vertabelo.com/blog/crow-s-foot-notation-in-vertabelo/)

Der findes forskellige måder at beskrive relationen mellem enheder, en af de mest benyttede hedder "Crow's foot notation"

Der findes 3 typer relationer.

- one to one
- one to many 
- many to many

Disse relationer kaldes *Cardinality* og angiver det maksimale antal gange en enhed kan forbindes til andre enheder.

Relationen angiver også minimum antal forbindelser til andre enheder, dette kaldes *modality* og kan enten være nul eller en.

Eksempler på crows foot notation kan ses herunder:  

![images/crows_foot_examples.png](images/crows_foot_examples.png)

Der er et par regler der er gode at kunne når primary key og foreign key skal angives i ER diagrammer:

| Relation | ER diagram |
| --- | --- |
| one-to-one | PK fra den ene tabel bliver til FK i den anden tabel, der er ikke vigtigt hvilken tabel der har FK |
| one-to-many | PK fra one tabellen bliver til FK i many tabellen |
| many-to-many | Denne relation vises kun i det logiske ER diagram. I det fysiske ER diagram skal relationen brydes ned i one-to-many relationer. Her bruges en forbindelses tabel som også kaldes en junction eller binding tabel. |

Når en tabel skal designes så bør disse regler overholdes:

1. Data skal kun findes i en tabel. Den samme data må ikke findes i flere tabeller.
2. Tabellen skal repræsentere et enkelt emne.
3. Der skal være en primary key der unikt identificerer hver række/entry i tabellen.
4. Entydige felter/fields som eksempel er dette ikke tilladt: "UCL, Seebladsgade 1, Odense C"
5. Felter skal kun indeholde en værdi, ikke "Jens, Holst, Andersen"
6. Ingen gentagne grupper, f.eks. Forfatter1, forfatter2, forfatter3.
7. Ingen felter der er afhængige af andre felter. F.eks er det bør fødselsdato gemmes frem for alder.  

Læs mere om tabel design her: [https://www.e-education.psu.edu/spatialdb/l2_p4.html](https://www.e-education.psu.edu/spatialdb/l2_p4.html)

## Instruktioner

1. Læs opgaven igennem

1. Læs om hvordan relationer mellem enheder angives med crows foot notation symboler [https://www.vertabelo.com/blog/crow-s-foot-notation/](https://www.vertabelo.com/blog/crow-s-foot-notation/)

2. Tegn et logisk ER diagram for:
   _En person kan have flere e-mail adresser_
3. Tegn et logisk ER diagram for:
   _En bog kan have flere forfattere men kun et forlag_
4. Tegn et logisk ER diagram for:
   _Et blog opslag er skrevet af en bruger men kan have kommentarer fra flere brugere der er logget ind_
5. Tegn et logisk ER diagram for:
   _Et fundraiser projekt kan have mange donationer fra mange donorer_

Når i er færdige med opgaven skal i have 4 logiske ER diagrammer.  
Husk at logiske ER diagrammer inkluderer attributter og relationer, men ikke datatyper, cardinality og modality!

![images/logic_er_diagram.png](images/logic_er_diagram.png)

## Links

- [https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model](https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model)
