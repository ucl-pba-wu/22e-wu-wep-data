---
hide:
  - footer
---

# Opgave 15 - Ejendomsmægler fysisk ER diagram

## Information

Denne opgave giver træning i at modellere en database fra kundens mockup til Fysisk ER diagram.

Opgaven er en gruppe opgave og i skal lave et fælles fysisk ER diagram som hele gruppen har adgang til.

Vi kigger på opgaven sammen næste gang.

## Instruktioner

En af jeres kunder vil have jer til at bygge en hjemmeside der viser alle huse som er til salg i Danmark.
Kunden har givet jer et delvist design som viser hvad de gerne vil kunne vise på siden.

1. Lav et fysisk ER diagram der kan gemme alt information fra kundens delvise design.

ps. Man kan gemme et hus på favoritlisten ved at klikke på hjertet.

![images/estate_agent.png](images/estate_agent.png)

## Links
