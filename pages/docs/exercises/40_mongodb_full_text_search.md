---
hide:
  - footer
---

# Opgave 40 - MongoDB full text search

## Information

MongoDB understøtter *full text search* som betyder at det er muligt at fremsøge dokumenter baseret på søgning i tekst felter.  
F.eks *summary* feltet i hbo kollektionen indeholder meget tekst.  
For at kunne søge i et felt er det nødvendigt at oprette et *text* indeks på det ønskede felt.  

## Instruktioner

**Del 1: Viden**

1. Læs om MongoDB full text search [https://www.mongodb.com/basics/full-text-search](https://www.mongodb.com/basics/full-text-search)
2. Læs om text indeks [https://www.mongodb.com/docs/manual/core/index-text/#overview](https://www.mongodb.com/docs/manual/core/index-text/#overview) 
3. Læs om text search [https://www.mongodb.com/docs/manual/reference/operator/query/text/](https://www.mongodb.com/docs/manual/reference/operator/query/text/)

**Del 2: Anvendelse**

Brug mongosh til at udføre kommandoer

1. Hent filen: [https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/hbo.js](https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/hbo.js) du kan højreklikke på den i din browser og vælge *gem som*
1. Lav en text søgning i hbo kollektionen `db.hbo.find({ $text: {$search: 'Theon'}})` og se hvilken fejl du får
2. Opret et text indeks på summary feltet i hbo kollektionen `db.hbo.createIndex({summary: 'text'})`
3. Bekræft at indekset er oprettet med `db.hbo.getIndexes()`
3. Eksperimenter med at søge på forskellige ting og også inkludere flere ord i don søgning, f.eks. `db.hbo.find({ $text: {$search: 'theon and arya'}})`
4. Slet dit indeks ved at angive navnet på dit indeks `db.hbo.dropIndex('summary_text')` 

## Links