---
hide:
  - footer
---

# Opgave 32 - Lovgivning og standardisering i IT sikkerhed

## Information

Lovgivning og standardisering er en stor del at it-sikkerhed.  

Lovgivning skal virksomheder leve op til, standardisering er et værktøj og et kvalitetsstempel flere, især større virksomheder, benytter sig af.

Standarder kan føre til ændret lovgivning og derfor kan det være en fordel for virksomheder at leve op til standarder inden det bliver til lov.

Overholdelsen af standarder kan også være et krav fra samarbejdspartnere, eller et krav hvis virksomheden arbejder med det offentlige.

Som webudvikler kan du komme til at arbejde med opgaver der skal overholde GDPR lovgivningen, det kan være du arbejder med en database der indeholder personfølsom information eller du implementerer funktionalitet i en webapplikation så brugere kan få en kopi af gemt person data og mulighed for at slette data (retten til at blive glemt).

### GDPR

GDRP står for *General Data Protection Regulation* og gælder for alle virksomheder der opererer i EU.  

Lovgivningen er lavet for at beskytte os alle mod misbrug af vores personlige data.  
Konsekvensen for en virksomhed der ikke efterlever GDPR kan være stor økonomisk i form af bøder, men også i forhold til virksomhedens omdømme.  

### DS/ISO27001

I europa er DS/ISO27001 standarden der benyttes når virksomheder vil implementere og vedligeholde deres sikkerhed. 
DS/ISO27001 kan være omfattende for små virksomheder at implementere. Derfor er det dog stadig vigtigt at de beskytter sig i en rimelig grad (hvilket mange desværre ikke gør)  
DS/ISO27001 dokumenterne har du adgang til via Dansk Standard Distribute og din UCL studie konto [https://sd.ds.dk/Account/LogOn](https://sd.ds.dk/Account/LogOn) 

### D-Mærket

D-Mærket er en mærkningsordningen på samme måde som e-mærket er det.  

D-Mærket er målrettet små- og mellemstore virksomheder og kan hjælpe med at sætte 
fokus på deres sikkerhedsniveau gennem blandt andet en selv evaluerings test.  

D-Mærket har fået kritik i it-sikkerhedsbranchen fordi det kan give en falsk følelse af sikkerhed hvis ikke virksomhedens sikkerhed evalueres og opdateres jævnligt.  
På trods af kritikken, synes jeg, at det er en god start og bedre end ingenting. 

### Privacy by design & default og Security by design & default

*Privacy by design (PbD) indebærer, at virksomheden tænker og bygger databeskyttelses- og privatlivsrelaterede værn ind i produkter og services fra udviklingens tidligste stadier og igennem hele livscyklussen.*

*Grundprincippet i security by design (SbD) er, at sikkerhed tænkes og bygges ind i produkter og tjenester fra udviklingens tidligste stadier og igennem hele livscyklussen.*  
*[Digital Tryghed - 2022](https://d-maerket.dk/kriterier/privacy-security-by-design-default/)*

Det er meget sandsynligt at du vil skulle arbejde med disse 2 begreber i din karriere og det er derfor godt at kende til dem.

## Instruktioner

1. Læs om indholdet i GDPR og hvilke krav virksomheder skal efterleve  
[https://gdpr.dk/databeskyttelsesforordningen/](https://gdpr.dk/databeskyttelsesforordningen/)
2. Læs om ISO27001  
[https://www.ds.dk/da/om-standarder/ledelsesstandarder/iso-27001-informationssikkerhed](https://www.ds.dk/da/om-standarder/ledelsesstandarder/iso-27001-informationssikkerhed)
3. Læs om D-Mærkets kriterier og reflekter over hvilke du synes har med web udvikling at gøre ?  
[https://d-maerket.dk/kriterier/](https://d-maerket.dk/kriterier/)

## Links
