---
hide:
  - footer
---

# Opgave 18 - JOIN, data fra flere tabeller

## Information

Indtil videre har de forespørgsler du har lavet kun hentet data fra en tabel.  
Når en database bruges i virkeligheden, vil det som regel være nødvendigt at kunne forespørge relateret data, fra flere tabeller.  
Til at gøre det bruges JOIN som en del af en SELECT statement.

Der findes flere typer af JOINS.

- Inner join
- Left outer join (left join)
- Right outer join (right join)
- Full outer join
- Cross join

Joins kan forklares ved at du forestiller dig at flere tabeller samles til en tabel hvor du kan forespørge efter data.
Måden de samles på afhænger af den type JOIN du anvender samt i hvilken rækkefølge du joiner tabellerne.

I denne opgave skal i som gruppe studere de forskellige typer JOIN og derefter anvende dem individuelt på en database der hedder Northwind

Northwind diagrammet er herunder

![images/northwind_schema.png](images/northwind_schema.png)

## Instruktioner

1. Læs disse artikeler om JOIN 
    - [https://medium.com/analytics-vidhya/sql-joins-visualized-d2f01909e07d](https://medium.com/analytics-vidhya/sql-joins-visualized-d2f01909e07d)
    - [https://www.codeproject.com/articles/33052/visual-representation-of-sql-joins](https://www.codeproject.com/articles/33052/visual-representation-of-sql-joins)
2. I gruppen snak om JOIN og få en fælles forståelse af hvordan JOIN virker i en forespørgsel.
2. Hent Northwind databasen på ITSLearning (individuelt)
3. Installer databasen (individuelt)
4. Hjælp hinanden med at lave følgende forespørgsler i Northwind databasen, i må ikke fortsætte til næste forespørgsel før alle har fået det til at virke.  
Gem forespørgslerne i en SQL script fil, som i til sidst deler.
    1. Vis ProductName og tilhørende CategoryName for alle produkter
    2. Vis alle produkter (ProductName, CompanyName, SupplierID) fra leverandøren 'Norske Meierier'
    3. Vis alle produktnavne der er med i ordre ID 10248
    4. Vis alle ordrer (OrderID, CustomerID, CompanyName, ContactName) der er håndteret af medarbejderen der hedder Andrew Fuller (FirstName, LastName)
    5. Vis alle medarbejderenavne (FirstName, LastName) og hvilket Territory de tilhører (ID + navn på Territory), sorter efter medarbejderens efternavn
    6. Vis alle medarbejderenavne (EmployeeID, fullname i en kolonne) der refererer til Steven Buchanan (fullname i en kolonne der hedder Boss Fullname)

**EKSTRA OPGAVE** *hvis i vil udfordres mere...*  
Hvor mange ordrer har hver leverandør ?  
Vis 2 kolonner, CompanyName og antal ordrer. Sorter faldende efter antal ordrer.

## Links

- På ITSLearning er der to forberedelses videoer der forklarer JOIN og giver et par eksempler på JOIN.
- Microsoft JOIN dokumentation [https://docs.microsoft.com/en-us/sql/relational-databases/performance/joins?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/relational-databases/performance/joins?view=sql-server-ver15)