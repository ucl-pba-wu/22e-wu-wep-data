---
hide:
  - footer
---

# Opgave 47 - Flere endpoints på todo API

## Information

I **Opgave 45 - mongodb node drivers** lavede du endpoints/routes til at hente alle tasks med `GET` og du lavede også et endpoint til at gemme en ny task med `POST`.

I denne opgave skal du lave endpoints til:

- `GET /tasks/id` til at hente en specifik task
- `PUT /tasks/id` til at opdatere en specifik task, her skal body i HTTP PUT indeholde et objekt med ændringer til den valgte task 
- `DELETE /tasks/id` til at slette en specifik task
 

Jeg har med vilje ikke lavet instruktioner som du "bare" kan kopiere :-)
Grunden er at jeg gerne vil have dig/jer til at bruge dokumentation og andre ressourcer til at implementere de nye endpoints, det er trods alt tættere på hvordan virkeligheden som webudvikler vil være for jer når i er færdige med uddannelsen.

Bare rolig, det er ikke meget anderledes at implementere de nye endpoints end dem der allerede er lavet og du skal kun lave ændringer i `TaskController.js`

Husk at du bruger express router og det `Task` objekt som er defineret i `/models/task`. Task objektet har mange metoder som du kan du får en liste over når du skriver `Task.`

![images/task_methods.png](images/task_methods.png)

Hvis du kører helt fast kan du smugkigge her [https://gitlab.com/ucl-pba-wu/node-mongo-todo/-/blob/013285bfac3e5faf5eae2c6f3c1aef413ea4df79/controllers/TaskController.js](https://gitlab.com/ucl-pba-wu/node-mongo-todo/-/blob/013285bfac3e5faf5eae2c6f3c1aef413ea4df79/controllers/TaskController.js)

Jeg har linket til relevant dokumentation og gode artikler nederst i opgaven

## Instruktioner

1. Lav et `GET` route/endpoint til `/tasks/id`, husk at håndtere HTTP status 200, 404 og 500 i response (res).  
`id` er et query parameter og kan hentes i `/tasks/id` metoden med `req.params.id`
2. Test med postman at du kan hente en specifik task
3. Lav et `PUT` route/endpoint til `/tasks/id`, husk at håndtere HTTP status 200 og 500 i response (res)  
`PUT` bruger både `req.prams.id` til at finde den rigtige task og ændringer hents fra request body parametret `req.body`.  
Task objektet har en metode der hedder `Task.findByIdAndUpdate()` som passer til formålet
4. Test med postman at du kan opdatere en specifik task
5. Lav et `DELETE` route/endpoint til `/tasks/id`, husk at håndtere HTTP status 200 og 500 i response (res).  
Task objektet har en metode der hedder `Task.findByIdAndRemove()` som passer til formålet
4. Test med postman at du kan slette en specifik task

## Links

- Express.js dokumentation (kig i `Getting started`, `Guide` og `API reference`) [https://expressjs.com/](https://expressjs.com/)
- Mongoose dokumentation [https://mongoosejs.com/docs/guide.html](https://mongoosejs.com/docs/guide.html)
- Net ninja complete mongodb tutorial (Brug video 20, 21 og 22) [https://youtube.com/playlist?list=PL4cUxeGkcC9h77dJ-QJlwGlZlTd4ecZOA](https://youtube.com/playlist?list=PL4cUxeGkcC9h77dJ-QJlwGlZlTd4ecZOA)  
Net ninja bruger `http PATCH` og jeg bruger `http PUT` til at opdatere, prøv dig frem med hvad du synes virker bedst og find selv ressourcer om hvad forskellen er