---
hide:
  - footer
---

# Opgave 25 - SQL views

## Information

Views i SQL er en gemt forespørgsel der kan genbruges. Views kaldes også for virtuelle tabeller fordi de, for brugeren af viewet, ser ud som en tabel i databsen, men i virkeligheden er det en foruddefineret forespørgsel der gemmes i databasen.

Views gemmes i databasen og kan ses i databasens view mappe i f.eks azure data studio

![images/views_folder.png](images/views_folder.png)

### View syntaks

Syntaksen for at oprette et view er ret simpel. Først skriver og tester du den forespørgsel du gerne vil gemme som et view

```SQL
/*Vis lande pr. continent (fra world databasen)*/
SELECT continent, COUNT(*) AS CountriesPerContinent 
FROM country 
GROUP BY continent;
```

Efterfølgende gemmer du forespørgslen som et navngivet view med `CREATE VIEW` kommandoen

```SQL
/*Opret view/virtuel tabel der viser lande pr. continent (fra world databasen)*/
CREATE VIEW vCountriesPerContinent AS
SELECT continent, COUNT(*) AS CountriesPerContinent 
FROM country 
GROUP BY continent;
```

Du kan forespørge i det gemte view som i enhver anden tabel

```SQL
SELECT * FROM vCountriesPerContinent
ORDER BY CountriesPerContinent ASC;
```

Et view kan også opdateres med `ALTER VIEW` kommandoen og slettes med `DELETE VIEW`

For at se hvad der gemmes i databasen når du opretter et view kan du skrive

```SQL
/*sp_helptext <view_navn>*/
sp_helptext vCountriesPerContinent
```

Som viser at det er en forespørgsel der er gemt i databasen

![images/views_what_is_stored.png](images/views_what_is_stored.png)



### Views, fordele og ulemper

Et view afvikler den gemte forespørgsel hver gang viewet bruges, det er godt fordi data altid er opdateret set fra brugerens side.  
Til gengæld kan det kræve mange ressourcer af databasen hvis forespørgslen er meget kompleks og dermed påvirke databasens performance/hastighed.

|Fordel|Ulempe|
| --- | --- |
|Komplekse forespørgsler gøres mere simple at anvende for brugere| Performance, views kører stadig en forespørgsel hvergang det afvikles |
|Der kan begrænses hvilke brugere der kan se hvad, f.eks kan følsom data fra en tabel gemmes ved at oprette et specifikt view til en bruger| Views er afhængige af de anvendte tabeller, hvis de ændres skal viewet også opdateres|
|Views kan oprettes som read only for udvalgte brugere, dermed mere sikkerhed for indhildet i databasen||
|Et view kan vise aggregeret istedet for detaljeret data ||
|Gamle tabeller kan erstattes af et view så databasen forbliver bagud kompatibel||

## Instruktioner

1. Se videoen fra Socratica om views (medmindre du allerede har set den i din forberedelse) [https://youtu.be/8jU8SrAPn9c](https://youtu.be/8jU8SrAPn9c)
2. Lav 3 views i World databasen (gem i en .sql fil), du må gerne genbruge gamle forespørgsler. 
3. Kig i databsen view mappe for at blive bekendt med placering og de informationer du kan få ud af det.
4. Undersøg et af dine views med `sp_helptext`
5. Opdater viewet i punkt 4 og kontroller at det er ændret med `sp_helptext`
6. Slet et af de 3 views

## Links

- Create view [https://learn.microsoft.com/en-us/sql/t-sql/statements/create-view-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/statements/create-view-transact-sql)
- Alter view [https://learn.microsoft.com/en-us/sql/t-sql/statements/alter-view-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/statements/alter-view-transact-sql)
- Drop view [https://learn.microsoft.com/en-us/sql/t-sql/statements/drop-view-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/statements/drop-view-transact-sql)