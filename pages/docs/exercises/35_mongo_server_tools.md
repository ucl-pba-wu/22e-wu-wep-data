---
hide:
  - footer
---

# Opgave 35 - MongoDB Server og værktøjer

## Information

MongoDB kan installeres på mange måder. Vi kommer i første omgang til at bruge en lokal installation af MongoDB og tilhørende værktøjer.

MongodDB kan sammenlignes med SQL Server, det er altså selve database serveren der menes nårvi omtaler MongoDB.  
Til at interagere med databasen bruger vi 2 forskellige værktøjer, et grafisk værktæj der hedder MongoDB Compass som kan sammenlignes med hhv. Azure Data Studio og SQL Server Managament Studio.
Vi skal også bruge et kommandolinje værktøj der hedder Mongosh som giver os mulighed for at interagere med vores databaser via kommandoer.

Hvis installationen driller så kan du bruge videoen fra net ninja [Complete MongoDB Tutorial #2 - Installing MongoDB](https://youtu.be/gDOKSgqM-bQ)

## Instruktioner

1. Installer MongoDB Community Server og MongoDB Compass (denne opgave bruger windows version 6.0.2 installeret som en service) fra [https://www.mongodb.com/docs/manual/administration/install-community/](https://www.mongodb.com/docs/manual/administration/install-community/) 
2. Installer Mongosh via denne guide, husk at vælge rigtig platform (denne opgave bruger windows version 1.6.0) [https://www.mongodb.com/docs/mongodb-shell/install/#std-label-mdb-shell-install](https://www.mongodb.com/docs/mongodb-shell/install/#std-label-mdb-shell-install) 
3. test at du kan forbinde til MongoDB Server fra både MongoDB Compass og Mongosh

## Links
