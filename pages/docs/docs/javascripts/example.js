// function concatenateTwo(word1, word2){
//     return word1+word2
// }

// result = concatenateTwo('I am ', 'a function result')

// // console.log(result)

// function stringReplacement(string_expression, string_pattern, string_replacement){
//     string_pattern = new RegExp(string_pattern, 'g')
//     return string_expression.replace(string_pattern, string_replacement)
// }

// str_exp = 'this text is abuot a woman and also abuot her cat'
// result = stringReplacement(str_exp, 'abuot', 'about')
// console.log(result)

let json_string = '{"name":"Jane","age":34,"address":{"street":"Seebladsgade 1","city":"Odense C"}}'
const student = JSON.parse(json_string)
console.log(student.name)
console.log(student.address.street)

JSON.stringify(student)
console.log(student)
