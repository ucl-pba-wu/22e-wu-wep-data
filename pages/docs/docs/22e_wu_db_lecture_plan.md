---
title: '22E PBa Web Udvikling'
subtitle: 'Fagplan for web programmering data'
filename: '22e_its_intro_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for web programmering data
skip-toc: false
semester: 22E
hide:
  - footer
---

# Introduktion

Fagområdet indeholder datalagring, -modellering, -udveksling af datakilder ud fra anerkendte standarder samt datasikkerhed.

Faget er på 10 ECTS point.

# Læringsmål

**Viden**

Den studerende har udviklingsbaseret om praksis, anvendt teori og udviklingsmetoder indenfor:

- Relevante internet og webprotokoller
- Datalagring, -modellering, -udveksling og -sikkerhed
- Kvalitetssikring

Den studerende kan forstå og reflektere over:

- Udviklingsmetoder inden for webudvikling
- Webarkitektur og designmønstre

**Færdigheder**

Den studerende kan:

- Mestre alle udviklingens faser herunder planlægge, udvikle og idriftsætte webapplikationer baseret på konkrete udviklingsønsker, samt vurdere praksisnære og teoretiske problemstillinger og vælge og begrunde relevante løsningsmodeller i relation til udviklingen af webapplikationer.
- Vurdere og begrunde valget af et egnet programmeringssprog og relevante metoder til implementering af webapplikationer
- Mestre et egnet programmeringssprog til udvikling af webapplikationer
- Anvende og modellere datakilder samt begrunde og formidle løsningsforslag
- Implementere og vurdere webbrugergrænseflader samt begrunde og formidle løsningsforslag til samarbejdspartnere og brugere
- Anvende relevante teorier og metoder til kvalitetssikring af alle udviklingens faser

**Kompetencer**

Den studerende kan:

- Håndtere kompleks webudvikling og skal kunne håndtere komplekse og udviklingsorienterede situationer i webudvikling
- Selvstændigt indgå i fagligt og tværfagligt samarbejde med en professionel tilgang og påtage sig ansvar inden for rammerne af en professionel etik i relation til webprogrammering
- Identificere og strukturere egne læringsbehov og udvikle egne færdigheder og kompetencer i relation til webprogrammering

Se [Studieordningen sektion 2.1](https://esdhweb.ucl.dk/D21-1632339.pdf)

# Lektionsplan

Se ITSLearning

# Studieaktivitets modellen

![study activity model](Study_Activity_Model.png)

## Andet

Intet på nuværende tidspunkt
