---
title: '22S ITS1 INTRO eksamens spørgsmål'
subtitle: 'Eksamens spørgsmål'
filename: '22S_ITS1_INTRO_eksamens_opgave'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2022-04-04
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamens spørgsmål
skip-toc: false
semester: 22S
hide:
  - footer
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _introduktion til IT sikkerhed_.

# Eksamens beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit 5.2.2_

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern censur.

For hvert spørgsmål bør den studerende forberede en præsentation/oplæg på max 10 minutter.  
Efter den studerendes præsentation stiller eksaminator og censor spørgsmål.  
Oplæg og spørgsmål tager samlet 20 minutter, de sidste 5 minutter er afsat til votering.

# Eksamens spørgsmål

1. ..
2. ..
3. ..
4. ..

# Eksamens datoer

- Forsøg 1 - 2022-11-15
- Forsøg 2 - 2022-12-01
- Forsøg 3 - 2023-01-12
