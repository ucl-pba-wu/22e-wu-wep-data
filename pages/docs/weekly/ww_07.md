---
Week: 41
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 41 - Sikkerhed

## Ugens emner 

- Gennemgang af Enquiry DB opgave
- DB Sikkerhed (authentificering, roller, rettigheder)
- GDPR
- Principle of least priviledge
- SQL injection
- DS/ISO27001
- Security By Design & Default
- Privacy By Design & Default
- OWASP

Som udvikler kan du ikke undgå at skulle forholde dig til IT sikkerhed.  

IT Sikkerhed er alles ansvar og især udviklere har en væsentlig rolle når de udvikler løsninger der opbevarer og behandler følsom information om kunder og ansatte, forretningshemmeligheder, brugernavne, passwords, mails, regnskaber og andet der kan skade virksomheden hvis de bliver offentliggjort eller slettet.  

Mange af de følsomme data gemmes i databaser og derfor er implementering af sikkerhed i databaser et must for udviklere.  
Det kan dog ikke begrænses til databasen alene, også applikationens frontend og især backend skal beskyttes.

Du vil lære om implementering af sikkerhed i SQL Server og hvordan hackere udfører angreb ved hjælp af SQL injections.  

Du vil få et indblik i den gældende lovgivning og standarder indenfor it sikkerhed og hvad begreber som Principle Of Least Priviledge, Security By Design & Default og Privacy By Design & Default er.  

Du får også præsenteret OWASP og deres top 10 over mest kritiske sårbarheder i web applikationer.


## Mål for ugen


### Praktiske mål

- Opgave 30-32 gennemført
- Obligatorisk lærings aktivitet på wiseflow

### Læringsmål

### Overordnede læringsmål fra studie ordningen

**Viden**

- Datalagring, -modellering, -udveksling og -sikkerhed
- Kvalitetssikring
- Udviklingsmetoder inden for webudvikling

**Færdigheder**

- Anvende og modellere datakilder samt begrunde og formidle løsningsforslag
- Mestre alle udviklingens faser herunder planlægge, udvikle og idriftsætte webapplikationer baseret på konkrete udviklingsønsker, samt vurdere praksisnære og teoretiske problemstillinger og vælge og begrunde relevante løsningsmodeller i relation til udviklingen af webapplikationer.
- Mestre et egnet programmeringssprog til udvikling af webapplikationer

**Kompetencer**

- Identificere og strukturere egne læringsbehov og udvikle egne færdigheder og kompetencer i relation til webprogrammering
- Håndtere kompleks webudvikling og skal kunne håndtere komplekse og udviklingsorienterede situationer i webudvikling

### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende har generel viden om det aktuelle trusselsbillede i forhold til webapplikationer
- Den studerende har viden om sårbarheder der relaterer til webapplikatione som anvender databaser
- Den studerende kan i nogen grad implementere sikkerhed i SQL server
- Den studerende har viden om hvilke dele af en webapplikations arkitektur der er sårbar
- Den studerende har viden om lovgivning og standarder der relaterer til it sikkerhed
- Den studerende har viden om bedst practice i webapplikationer (security, privacy, OWASP top 10)
- Den studerende er bevidst om udviklerens rolle i forhold til implementering og vedligeholdelse af sikkerhed i web applikationer 
- Den studerende kan i nogen grad vurdere og afprøve sikkerhed i webapplikationer  

## Afleveringer

- Obligatorisk lærings aktivitet på wiseflow

## Skema

Herunder er det planlagte program for ugen.

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- ..
