---
hide:
  - footer
---

# UCL PBa Webudvikling - Web Programmering (Data)

På dette website finder du ugeplaner, opgaver, dokumenter og links til brug i faget _Web Programmering (Data)_ Efterår 2022

Fagområdet indeholder datalagring, -modellering, -udveksling af datakilder ud fra anerkendte standarder samt datasikkerhed.


# Læringsmål

## Viden

**Den studerende har udviklingsbaseret viden om praksis, anvendt teori og udviklingsmetoder indenfor:**

- relevante internet og webprotokoller
- datalagring, -modellering, -udveksling og -sikkerhed
- kvalitetssikring

**Den studerende kan forstå og reflektere over:**

- udviklingsmetoder inden for webudvikling
- webarkitektur og designmønstre

## Færdigheder

**Den studerende kan:**

- mestre alle udviklingens faser herunder planlægge, udvikle og idriftsætte webapplikationer baseret på konkrete udviklingsønsker, samt vurdere praksisnære og teoretiske problemstillinger og vælge og begrunde relevante løsningsmodeller i relation til udviklingen af webapplikationer.
- vurdere og begrunde valget af et egnet programmeringssprog og relevante metoder til implementering af webapplikationer,
- mestre et egnet programmeringssprog til udvikling af webapplikationer
- anvende og modellere datakilder samt begrunde og formidle løsningsforslag
- implementere og vurdere webbrugergrænseflader samt begrunde og formidle løsningsforslag til samarbejdspartnere og brugere
- anvende relevante teorier og metoder til kvalitetssikring af alle udviklingens faser

## Kompetencer

**Den studerende kan:**

- håndtere kompleks webudvikling og skal kunne håndtere komplekse og udviklingsoriente-rede situationer i webudvikling
- selvstændigt Indgå i fagligt og tværfagligt samarbejde med en professionel tilgang og påtage sig ansvar inden for rammerne af en professionel etik i relation til webprogrammering
- identificere og strukturere egne læringsbehov og udvikle egne færdigheder og kompetencer i relation til webprogrammering
