// 1.Vis air date og air time på episoden ved navn "Winter is coming"
db.hbo.find({name: "Winter is Coming"}, {airdate: 1, airtime: 1})

// 2.Vis navnet på episode 5, sæson 2. Vis kun navnet. 
db.hbo.find({season: 2, number: 5}, {name: 1})

// 3.Vis id for det dokument hvor feltet "original image" indeholder strengen '2669.jpg'​
db.hbo.find({ "image.medium": { $regex: "2669.jpg" } }, {_id: 1})

// 4.Vis navne på alle episoder der har en runtime mellem 50 og 60 minutter (begge tal inklusiv). Sorter dem faldende efter navn.  
db.hbo.find({ runtime: { $gte: 50, $lte: 60 } }, {name: 1 }).sort({name: -1})

// 5.Vis hvor mange episoder der er pr. sæson, grupper og sorter efter sæson stigende.
db.hbo.aggregate([
    {$group : {_id : "$season", episodes: {$sum: 1}}},
    {$sort : { count: 1}}        
])

// 6.Vis gennemsnits runtime af episoderne pr. sæson
db.hbo.aggregate([{ $group: { _id: "$season", avg: { $avg: "$runtime" } } }]).sort({_id: 1})
