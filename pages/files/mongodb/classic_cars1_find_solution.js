// classic_cars Client collection
use classic_cars

// vis hele client collection
db.client.find()

// Find alle biler for personer med fornavnet "Gaston"
db.client.find({"First_Name": "Gaston"}, {Cars: 1, _id: 0});

// Vis den by Paul Miller bor i
db.client.find({"First_Name": "Paul", "Surname": "Miller"}, {City: 1, _id: 0})

// Find alle personer med et efternavn der starter med "B"
db.client.find({"Surname": /^B/})

// Find alle der ejer en "Renault"
db.client.find({"Cars.Model": "Renault"},{Surname: 1, First_Name: 1, Cars: 1})

// Find alle personer der bor i enten Zurich eller Rome
db.client.find({$or: [{City: "Zurich"}, {City: "Rome"}]})

// Tæl hvor mange biler der er bygget i 1999
db.client.count({"Cars.Year": "1999"})
db.client.find({"Cars.Year": "1999"}).count()

// Split Cars array
db.client.aggregate([{$unwind: "$Cars"}])