// Update

db.inventory.insertMany([
  { item: "canvas", qty: 100, size: { h: 28, w: 35.5, uom: "cm" }, status: "A" },
  { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
  { item: "mat", qty: 85, size: { h: 27.9, w: 35.5, uom: "cm" }, status: "A" },
  { item: "mousepad", qty: 25, size: { h: 19, w: 22.85, uom: "cm" }, status: "P" },
  { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "P" },
  { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
  { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
  { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
  { item: "sketchbook", qty: 80, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
  { item: "sketch pad", qty: 95, size: { h: 22.85, w: 30.5, uom: "cm" }, status: "A" }
]);

// Update one field
db.inventory.updateOne(
  { status: "A" },
  {
    $set: { "status": "X" }
  }
)

// Update two fields and add lastModified date
db.inventory.updateOne(
  { item: "paper" },
  {
    $set: { "size.uom": "cm", status: "P" },
    $currentDate: { lastModified: true }
  }
)

// Replace document
db.inventory.replaceOne(
  { item: "paper" },
  { item: "paper", instock: [{ warehouse: "A", qty: 60 }, { warehouse: "B", qty: 40 }] }
)


// Update - add field

db.places.insertMany([
  { "_id": 1, "name": "Central Perk Cafe", "violations": 3 },
  { "_id": 2, "name": "Rock A Feller Bar and Grill", "violations": 2 },
  { "_id": 3, "name": "Empire State Sub", "violations": 5 },
  { "_id": 4, "name": "Pizza Rat's Pizzaria", "violations": 8 }
])

db.places.updateMany(
  { violations: { $gt: 4 } },
  { $set: { "Review": true } }
);


// Update with Insert

db.inspectors.insertMany([
  { "_id": 92412, "Inspector": "F. Drebin", "Sector": 1, "Patrolling": true },
  { "_id": 92413, "Inspector": "J. Clouseau", "Sector": 2, "Patrolling": false },
  { "_id": 92414, "Inspector": "J. Clouseau", "Sector": 3, "Patrolling": true },
  { "_id": 92415, "Inspector": "R. Coltrane", "Sector": 3, "Patrolling": false }
])

db.inspectors.updateMany(
  { "Sector": { $gt: 4 }, "Inspector": "R. Coltrane" },
  { $set: { "Patrolling": false } },
  { upsert: true }
);


// Update or insert new record if doesn't exist
db.client.updateOne(
  { "firstname": "Paul" },
  { $set: { "firstname": "Paul", "surname": "Wright" } },
  { upsert: true }
)

// Add to array
db.client.updateOne(
  { "firstname": "Paul", "surname": "Wright" },
  { $push: { "cars.type": { "model": "BMW", "year": 2010 } } }
)

// Remove field
db.client.updateMany(
  { surname: "Ortega" },
  { $unset: { city: "" } }
)


// Increment field
db.sku.insertOne({
  _id: 1,
  sku: "abc123",
  quantity: 10,
  metrics: {
    orders: 2,
    ratings: 3.5
  }
})

db.sku.update(
  { sku: "abc123" },
  { $inc: { quantity: -2, "metrics.orders": 1 } }
)

// -----------
// AGGREGATION

db.books.insertMany([
  { "_id": 8751, "title": "The Banquet", "author": "Dante", "copies": 2 },
  { "_id": 8752, "title": "Divine Comedy", "author": "Dante", "copies": 1 },
  { "_id": 8645, "title": "Eclogues", "author": "Dante", "copies": 2 },
  { "_id": 7000, "title": "The Odyssey", "author": "Homer", "copies": 10 },
  { "_id": 7020, "title": "Iliad", "author": "Homer", "copies": 10 }
])


// Group by author
db.books.aggregate(
  [
    { $group: { _id: "$author", count: { $sum: 1 } } }
  ]
)

// Sum of books per Author
db.books.aggregate(
  [
    { $group: { _id: "$author", count: { $sum: "$copies" } } }
  ]
)

// Rename output
db.books.aggregate(
  [
    { $group: { _id: "$author", count: { $sum: "$copies" } } },
    { $project: { _id: 0, count: 1, author: "$_id" } }
  ]
)

// Total sum of all rows
db.books.aggregate(
  [
    { $group: { _id: null, count: { $sum: "$copies" } } }
  ]
)

db.books.aggregate(
  [
    { $group: { _id: "$author", books: { $push: "$title" } } }
  ]
)


// Unwind aggregation stage
// Create a document for each array item

// Count cars for 'Paul'
db.clients.aggregate([
  { $match: { firstname: "Paul" } },
  { $project: { cars: true, _id: false } },
  { $unwind: "$cars" },
  { $count: "cars" }
])
