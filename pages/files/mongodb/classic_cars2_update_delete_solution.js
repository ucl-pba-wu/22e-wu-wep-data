// 1. Indsæt 3 nye clients i *classic cars* databasen, de skal alle have efternavnet Deleteme 

db.client.insertMany([
    {"Pers_ID": "5", "Surname": "Deleteme", "First_Name": "Johnny", "City": "Liege", "Cars":[{"Model": "Bently", "Year": "1982", "Value": "100000"}, {"Model": "Rolls Royce", "Year": "1965", "Value": "330000"}]},
    {"Pers_ID": "6", "Surname": "Deleteme", "First_Name": "Coolio", "City": "Zaragozza", "Cars":[{"Model": "Porche", "Year": "2000", "Value": "2000000"}]},
    {"Pers_ID": "7", "Surname": "Deleteme", "First_Name": "Octo", "City": "Madrid", "Cars":[{"Model": "Skoda", "Year": "2013", "Value": "50000"}]},
    ])
// check
db.client.find({Surname: "Deleteme"})


// 2. Opdater en valgfri client og indsæt en bil mere i Cars arrayet
db.client.updateOne({"First_Name": "Johnny"}, {$set: { "Cars": {"Model": "Skoda", "Year": "2013", "Value": "50000"}}})
// check
db.client.find({First_Name: "Johnny"})

// 3. Opdater alle clients der starter med b, til at bo i Odense
db.client.updateMany({"Surname": /^D/}, {$set: { "City": "Odense"}})
// check
db.client.find({City: "Odense"})

// 4. Slet alle clients der bor i Odense
db.client.deleteMany({"City": "Odense"})
db.client.find({City: "Odense"})