USE BikeStores;
GO

SELECT * FROM production.products;

-- How many customers are there?
SELECT DISTINCT(COUNT(*))
FROM sales.customers;

-- Show all orders where shipped date was more then 2 day after the required date
SELECT *
FROM sales.orders AS o
WHERE DATEDIFF(DAY, o.shipped_date, o.required_date) > 1

-- What is the average list price of products. (Show result in whole numbers).
SELECT CAST(ROUND(AVG(list_price),0) AS INT)
FROM production.products

-- How many customers are there per state
SELECT [state], Count(*) AS customerPerState
FROM sales.customers
GROUP BY [state]

-- Show the number of products per Model Year
SELECT model_year, count(*) AS numberOfProducts 
FROM production.products
GROUP BY model_year

-- Show the brand name and the number of products per brand. Show highest count first
SELECT b.brand_name, count(*) as NumberOfProducts
FROM production.products AS p
INNER JOIN production.brands AS b
    ON p.brand_id = b.brand_id
GROUP BY b.brand_name
ORDER By NumberOfProducts DESC


-- BONUS question: Display order and total price of the order
SELECT o.order_id, SUM(quantity * (1-discount) * list_price) AS totalprice
FROM sales.orders AS o
INNER JOIN sales.order_items AS i
    ON o.order_id = i.order_id
GROUP BY o.order_id
