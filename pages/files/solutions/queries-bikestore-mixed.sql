USE BikeStores;

-- Show how many employees work PER store
SELECT s.store_id, COUNT(*) NumOfEmployees
FROM sales.staffs s
GROUP BY s.store_id

-- What price of the the most expensive product? Round it UP to closest whole number.
SELECT CEILING(MAX(list_price)) MostExpensiveProduct
FROM production.Products

-- Show customer's firstname and lastname and their initials e.g. Deborah Burks (DB)
SELECT first_name, last_name, CONCAT('(', SUBSTRING(first_name, 1,1), SUBSTRING(last_name, 1,1), ')') AS Initials
FROM sales.customers

-- Show the number of products per brand_id. Show only brands that have more than 100 products
SELECT p.brand_id, COUNT(*) productsPerCategory
FROM production.products p
GROUP BY p.brand_id
HAVING COUNT(*) > 100

-- Same query as before, but now show the name of the category as wellShow the number of products per category id. Show only categories that have more than 50 products
SELECT b.brand_name, COUNT(*) productsPerCategory
FROM production.products p
INNER JOIN production.brands b
ON p.brand_id = b.brand_id
GROUP BY b.brand_name
HAVING COUNT(*) > 100

-- How many orders where placed by the employee with the name of Mireya Copeland.
SELECT Count(*) numOforders
FROM sales.orders o
INNER JOIN sales.staffs s
    ON o.staff_id = s.staff_id
WHERE first_name = 'Mireya' AND last_name = 'Copeland'

-- Show the each store name and the total quantity of products they have in store.
SELECT * from sales.stores

SELECT store_name, SUM(quantity) as TotalProductsPerStore
FROM production.stocks stock
INNER JOIN sales.stores store
    ON stock.store_id = store.store_id
GROUP BY store_name

-- Show all the products in the category 'Comfort Bicycles'. Show product name only, ordered alphabetically
SELECT p.product_name
FROM production.products p
INNER JOIN production.categories c
    ON p.category_id = c.category_id
WHERE c.category_name = 'Comfort Bicycles'
ORDER BY p.product_name 









SELECT *
FROM production.products



SELECT *
FROM production.categories