USE World;
GO

/* vis totalt overfladeareal pr. region, sorter faldende*/
SELECT Region, SUM(SurfaceArea) as TotalSurfaceArea
FROM Country
GROUP BY Region
ORDER BY TotalSurfaceArea DESC;

/*Vis forventet levealder stigende i en kolonne, vis lande med den pågældende levealder i en anden */
SELECT LifeExpectancy, STRING_AGG(TRIM(Name), ', ') AS CountryNames
FROM Country
GROUP BY LifeExpectancy
HAVING LifeExpectancy > 0
ORDER BY LifeExpectancy ASC;


/*Vis, i stigende rækkefølge, regioner med en samlet befolkning under 8 millioner*/
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) < 8000000
ORDER BY SumPerRegion ASC;


/*Vis, i faldende rækkefølge efter befolkningstal, regioner med en samlet befolkning under 5 millioner*/
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) < 5000000
ORDER BY SumPerRegion DESC;