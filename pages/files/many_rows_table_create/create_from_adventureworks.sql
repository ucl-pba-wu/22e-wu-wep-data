USE [AdventureWorksDW2019]
GO
--Change 1000000 to the number of your preference for your needs
SELECT TOP 10000000
      c1.[FirstName],
	  c2.[LastName],
      c3.[BirthDate]
 
  FROM [dbo].[DimCustomer] c1
CROSS JOIN
DimCustomer c2
CROSS JOIN
DimCustomer c3