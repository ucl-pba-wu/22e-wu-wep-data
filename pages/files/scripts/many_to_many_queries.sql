SELECT Book.Title
FROM Author
INNER JOIN BookHasAuthor 
ON Author.ID = BookHasAuthor.AuthorID
INNER JOIN Book
ON BookHasAuthor.BookID = Book.ID
WHERE Author.Firstname = 'Hans'

SELECT Author.Firstname
FROM Author
INNER JOIN BookHasAuthor 
ON Author.ID = BookHasAuthor.AuthorID
INNER JOIN Book
ON BookHasAuthor.BookID = Book.ID
WHERE Book.Title = 'Det fors�mte for�r'

SELECT *
FROM BookHasAuthor

SELECT *
FROM Book