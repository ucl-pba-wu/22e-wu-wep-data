CREATE DATABASE HumanResourcesDB
GO

USE HumanResourcesDB

CREATE TABLE Employee (
    ID INT IDENTITY PRIMARY KEY,
    Firstname VARCHAR(50) NOT NULL,
    JobTitle VARCHAR(50),
    ManagerID INT FOREIGN KEY REFERENCES Employee(ID)
)
GO

INSERT INTO Employee (Firstname, JobTitle, ManagerID)
VALUES ('Kim','Manager', 1)
INSERT INTO Employee (Firstname, JobTitle, ManagerID)
VALUES ('Bernie','Adjunkt', 2)
INSERT INTO Employee (Firstname, JobTitle, ManagerID)
VALUES ('Morten','Adjunkt', 2)
INSERT INTO Employee (Firstname, JobTitle, ManagerID)
VALUES ('Dorthe','Head of Department', NULL)

GO
