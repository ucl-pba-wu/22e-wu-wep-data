USE MASTER;
GO

CREATE DATABASE People;

USE People;
GO

CREATE TABLE persons (
    id int NOT NULL IDENTITY(1, 1),
    first_name nvarchar(50)  NOT NULL,
    last_name nvarchar(50) NOT NULL,
    birth_date date NULL
);

INSERT INTO persons (first_name, last_name) VALUES ('King', 'Kong');

SELECT LastName FROM customers

CREATE NONCLUSTERED INDEX iNames
ON persons(first_name, last_name);

SELECT COUNT(*) 
FROM customers
WHERE LastName = 'Patel'

DROP INDEX iFirstName
ON persons;

DROP TABLE persons;


DROP DATABASE People;