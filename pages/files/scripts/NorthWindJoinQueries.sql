/* a. Vis ProductName og tilhørende CategoryName for alle produkter */

SELECT * FROM Products;
SELECT * FROM categories;

SELECT ProductName, CategoryName
FROM products
INNER JOIN Categories
ON Products.CategoryID = Categories.CategoryID;

/* b. Vis alle produkter fra leverandøren 'Norske Meierier' */

SELECT * FROM Suppliers
SELECT * FROM Products

SELECT ProductName, Products.SupplierID AS p_supplierid, Suppliers.SupplierID AS s_supplierid, Suppliers.CompanyName
FROM products
INNER JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID
WHERE Suppliers.CompanyName = 'Norske Meierier';

/* c. Vis alle produktnavne der er med i ordre ID 10248 */

SELECT * FROM [Order Details] WHERE OrderID = 10248

SELECT ProductName
FROM [Order Details]
INNER JOIN Products
ON [Order Details].ProductID = Products.ProductID
WHERE OrderID = 10248;

/* d. Vis alle ordrer (OrderID, CustomerID, CompanyName, ContactName) der er håndteret af medarbejderen der hedder Andrew Fuller (FirstName, LastName) */

SELECT OrderID, Customers.CustomerID, CompanyName, Contactname, CONCAT(FirstName, ' ', LastName) as 'Handled By'
FROM Orders
INNER JOIN Customers
ON orders.CustomerID = customers.CustomerID
INNER JOIN Employees
ON Orders.EmployeeID = Employees.EmployeeID
WHERE Employees.FirstName = 'Andrew' AND Employees.LastName = 'Fuller'
ORDER BY OrderID;

/* e. Vis alle medarbejdere der refererer til Steven Buchanan */

SELECT FirstName, LastName, EmployeeID, ReportsTo FROM Employees

SELECT Emp.EmployeeID, CONCAT(Emp.FirstName, ' ', Emp.LastName) as 'Employee FullName', CONCAT(Boss.FirstName, ' ', Boss.LastName) as 'Reports to' 
FROM Employees AS Emp
INNER JOIN Employees AS Boss 
ON Boss.EmployeeID = Emp.ReportsTo
WHERE Boss.FirstName = 'Steven' AND Boss.LastName = 'Buchanan'

SELECT Emp.EmployeeID, CONCAT(Emp.FirstName, ' ', Emp.LastName) as 'Employee FullName', Boss.FirstName + ' ' + Boss.LastName as 'Reports to' 
FROM Employees AS Emp
INNER JOIN Employees AS Boss 
ON Boss.EmployeeID = Emp.ReportsTo
WHERE Boss.FirstName = 'Steven' AND Boss.LastName = 'Buchanan'

/* f. Vis alle medarbejderenavne (FirstName, LastName) og hvilket Territory de tilhører (ID + navn på Territory), sorter efter medarbejderens efternavn */

SELECT Employees.FirstName, Employees.LastName, Territories.TerritoryDescription, Territories.TerritoryID
FROM Employees
INNER JOIN EmployeeTerritories
ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
INNER JOIN Territories
ON EmployeeTerritories.TerritoryID = Territories.TerritoryID
ORDER BY Employees.LastName

/* Hvor mange ordrer har hver leverandør ? Vis CompanyName og antal ordrer, vis faldende efter antal ordrer */

SELECT Suppliers.CompanyName, count(orders.OrderID) AS OrderCount
FROM Suppliers
INNER JOIN products
ON Suppliers.SupplierID = Products.SupplierID
INNER JOIN [Order Details]
ON products.ProductID = [Order Details].ProductID
INNER JOIN Orders
ON [Order Details].OrderID = Orders.OrderID
GROUP BY suppliers.supplierID, Suppliers.CompanyName
ORDER BY ordercount DESC;