USE World;
GO

SELECT GETDATE() as SystemDate;

SELECT DATEDIFF(year, '2018-12-31', '2019-05-01') as DateDiff;


SELECT continent, COUNT(*) AS CountriesPerContinent 
FROM country 
GROUP BY continent;  

SELECT region, SUM(population) AS SumPerRegion 
FROM country 
GROUP BY region 
ORDER BY SumPerRegion DESC;

SELECT countrycode, SUM(percentage) AS SumOfficialLang 
FROM countrylanguage 
WHERE isofficial = 'T' 
GROUP BY countrycode;  

SELECT IsOfficial, COUNT(*) 
FROM countrylanguage 
GROUP BY IsOfficial;

SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) > 300000000
ORDER BY SumPerRegion DESC;

SELECT *
FROM City
GROUP BY CountryCode;

SELECT CountryCode
FROM City
GROUP BY CountryCode;

SELECT * FROM City

SELECT CountryCode, COUNT(*) AS CitiesPerCountry
FROM City
GROUP BY CountryCode;

SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SumPerRegion > 300000000
ORDER BY SumPerRegion DESC;

SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) > 300000000
ORDER BY SumPerRegion DESC;

SELECT * FROM Country
SELECT * FROM City

/* vis totalt overfladeareal pr. verdensdel, sorter faldende*/
SELECT Region, SUM(SurfaceArea) as TotalSurfaceArea
FROM Country
GROUP BY Region
ORDER BY TotalSurfaceArea DESC;

/*Vis forventet levealder stigende i en kolonne, vis lande med den pågældende levealder i en anden */
SELECT LifeExpectancy, STRING_AGG(TRIM(Name), ', ') AS CountryNames
FROM Country
GROUP BY LifeExpectancy
HAVING LifeExpectancy > 0
ORDER BY LifeExpectancy ASC;


/*Vis, i stigende rækkefølge, regioner med en samlet befolkning under 8 millioner*/
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) < 8000000
ORDER BY SumPerRegion ASC;
