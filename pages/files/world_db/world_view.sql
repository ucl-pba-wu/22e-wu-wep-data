USE World;
GO

SELECT GETDATE() as SystemDate;

SELECT DATEDIFF(year, '2018-12-31', '2019-05-01') as DateDiff;

CREATE VIEW vCountriesPerContinent AS
SELECT continent, COUNT(*) AS CountriesPerContinent 
FROM country 
GROUP BY continent; 

SELECT * FROM vCountriesPerContinent
ORDER BY CountriesPerContinent ASC;

sp_helptext vCountriesPerContinent 

UPDATE STATISTICS vCountriesPerContinent