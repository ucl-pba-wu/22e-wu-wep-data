-- -- create procedure

-- CREATE PROCEDURE spGetLanguages
-- AS
-- BEGIN
-- 	SELECT DISTINCT language 
-- 	FROM CountryLanguage
-- END;

-- -- run procedure

-- EXECUTE spGetLanguages;

-- -- alter procedure

-- ALTER PROCEDURE spGetLanguages
-- AS
-- BEGIN
-- 	SELECT DISTINCT language 
-- 	FROM CountryLanguage
-- 	WHERE Language like 'A%'
-- END;

-- -- Stored procedure with variable

-- CREATE PROCEDURE spGetLanguageByMinPercentage
-- @MinPercent DECIMAL(4,1)
-- AS
-- BEGIN
-- 	SELECT * 
-- 	FROM CountryLanguage
-- 	WHERE Percentage >= @MinPercent
-- END;

-- -- Execute stored procedure with variable

-- EXECUTE spGetLanguageByMinPercentage 70.0;
-- EXECUTE spGetLanguageByMinPercentage 50.0;

-- -- Stored procedure with 2 variable

-- CREATE PROCEDURE spGetLanguageByMinPercentageAndOfficial
-- @MinPercent DECIMAL(4,1),
-- @IsOfficial CHAR(1)
-- AS
-- BEGIN
-- 	SELECT * 
-- 	FROM CountryLanguage
-- 	WHERE Percentage >= @MinPercent
-- 	AND IsOfficial = @IsOfficial
-- END

-- -- Execute stored procedure with 2 variables, variables has to follow order of parameterlist

-- EXECUTE spGetLanguageByMinPercentageAndOfficial 70.0, 'F'
-- EXECUTE spGetLanguageByMinPercentageAndOfficial 50.0, 'T'

-- -- Named parameters in call if changing order
-- EXECUTE spGetLanguageByMinPercentageAndOfficial  @IsOfficial='T', @MinPercent=50.0


-- CREATE PROCEDURE spGetCountryNameByCode
-- @Code CHAR(3)
-- AS
-- BEGIN
--    SELECT Name FROM Country WHERE Code = @Code
-- END

-- EXECUTE spGetCountryNameByCode

-- Select * from country

-- SELECT *
-- FROM City
-- INNER JOIN Country
-- 	ON City.CountryCode = Country.Code
-- WHERE Country.Code = 'DNK' AND city.Population >= 100000

-- CREATE PROCEDURE spGetCitiesByCountryAndMinPop
-- @CountryCode CHAR(20),
-- @Pop int
-- AS
-- BEGIN
--    SELECT *
--    FROM City
--    INNER JOIN Country
--        ON City.CountryCode = Country.Code
--    WHERE Country.Code = @CountryCode AND city.Population >= @Pop
-- END

-- EXEC spGetCitiesByCountryAndMinPop 'DNK', '100000'

-- EXEC spGetCitiesByCountryAndMinPop 'DNK', 100000

-- EXEC spGetCitiesByCountryAndMinPop 100000, 'DNK'

-- sp_helptext spGetCitiesByCountryAndMinPop

-- sp_helptext spGetCountryNameByCode


/* Lav følgende om til en stored procedure med parametre*/

-- SELECT region, SUM(population) AS SumPerRegion
-- FROM country
-- GROUP BY region
-- HAVING SUM(population) > 300000000
-- ORDER BY SumPerRegion DESC;

-- CREATE PROCEDURE spSumPerRegion
-- @SumPerRegion INT
-- AS
-- BEGIN
-- SELECT region, SUM(population) AS SumPerRegion
-- FROM country
-- GROUP BY region
-- HAVING SUM(population) > @SumPerRegion
-- ORDER BY SumPerRegion DESC;
-- END

EXEC spSumPerRegion 100000
EXEC spSumPerRegion

-- USE Northwind;
-- GO

-- SELECT OrderID, Customers.CustomerID, CompanyName, Contactname, CONCAT(FirstName, ' ', LastName) as 'Handled By'
-- FROM Orders
-- INNER JOIN Customers
-- ON orders.CustomerID = customers.CustomerID
-- INNER JOIN Employees
-- ON Orders.EmployeeID = Employees.EmployeeID
-- WHERE Employees.FirstName = 'Andrew' AND Employees.LastName = 'Fuller'
-- ORDER BY OrderID;

-- CREATE PROCEDURE spHandledByEmployee
-- @EmpFirstName NVARCHAR(10),
-- @EmpLastName NVARCHAR(20)
-- AS
-- BEGIN
-- SELECT OrderID, Customers.CustomerID, CompanyName, Contactname, CONCAT(FirstName, ' ', LastName) as 'Handled By'
-- FROM Orders
-- INNER JOIN Customers
-- ON orders.CustomerID = customers.CustomerID
-- INNER JOIN Employees
-- ON Orders.EmployeeID = Employees.EmployeeID
-- WHERE Employees.FirstName = @EmpFirstName AND Employees.LastName = @EmpLastName
-- ORDER BY OrderID;
-- END

-- SELECT * FROM Employees

-- EXEC spHandledByEmployee Janet, Leverling