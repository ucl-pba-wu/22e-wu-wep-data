-- create procedure

CREATE PROCEDURE spGetLanguages
AS
BEGIN
	SELECT DISTINCT language 
	FROM CountryLanguage
END

-- run procedure

EXECUTE spGetLanguages

-- alter procedure

ALTER PROCEDURE spGetLanguages
AS
BEGIN
	SELECT DISTINCT language 
	FROM CountryLanguage
	WHERE Language like 'A%'
END

-- Stored procedure with variable

CREATE PROCEDURE spGetLanguageByMinPercentage
@MinPercent DECIMAL(4,1)
AS
BEGIN
	SELECT * 
	FROM CountryLanguage
	WHERE Percentage >= @MinPercent
END

-- Execute stored procedure with parameter

EXECUTE spGetLanguageByMinPercentage 70.0
EXECUTE spGetLanguageByMinPercentage 50.0

-- Stored procedure with 2 paramters

CREATE PROCEDURE spGetLanguageByMinPercentageAndOfficial
@MinPercent DECIMAL(4,1),
@IsOfficial CHAR(1)
AS
BEGIN
	SELECT * 
	FROM CountryLanguage
	WHERE Percentage >= @MinPercent
	AND IsOfficial = @IsOfficial
END

-- Execute stored procedure with 2 variables, variables has to follow order of parameterlist

EXECUTE spGetLanguageByMinPercentageAndOfficial 70.0, 'F'
EXECUTE spGetLanguageByMinPercentageAndOfficial 50.0, 'T'

-- Named parameters in call if changing order
EXECUTE spGetLanguageByMinPercentageAndOfficial  @IsOfficial='F', @MinPercent=60.0

-- System stored procedures

-- Get info on country table
EXECUTE sp_help country
EXECUTE sp_columns country

EXECUTE sp_helptext spGetLanguageByMinPercentageAndOfficial

-- Get country by region

CREATE PROCEDURE spGetCountryByRegion
@Region Char
AS
BEGIN
	SELECT * 
	FROM Country
	WHERE Region >= @Region
END

ALTER PROCEDURE spGetCountryByRegion
@Region Char(26)
AS
BEGIN
	SELECT * 
	FROM Country
	WHERE Region = @Region
END

EXECUTE spGetCountryByRegion 'Caribbean'
EXECUTE spGetCountryByRegion 'Central Africa'