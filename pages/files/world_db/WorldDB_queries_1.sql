USE World;
GO;

/* Vis bynavn og landekode for alle byer i Holland (NLD) */ 

SELECT * FROM City;

SELECT Name, CountryCode, ID 
FROM City
WHERE CountryCode = 'AUS'

/* Find alle byer i Hubei distriktet der har et navn der starter med X (4 rows)*/
  
SELECT *
FROM city
WHERE district IN ('Hubei') AND name LIKE 'X%'
ORDER BY name DESC;

/* Find alle byer i Hubei distriktet der har et navn der starter med X (4 rows)*/

SELECT *
FROM city
WHERE district IN ('Zuid-Holland') AND name LIKE '%t'
ORDER BY Population DESC;
  
/* Vis en top 10 over byer med flest indbyggere, kun navn og befolkningstal skal vises */
  
SELECT TOP 10 name, Population
FROM City
ORDER BY population DESC; 

SELECT name, Population
FROM City
ORDER BY population ASC;
  
/* Vis alle byer i La Paz, West Java og Limburg, sorter efter A-Z (27 rows)*/

SELECT name, district
FROM city
WHERE district IN ('La Paz', 'West Java', 'Limburg')
ORDER BY name DESC;

SELECT *
FROM City
WHERE District in ('Paz', 'West Java', 'Limburg')
ORDER BY name DESC;
  
/* Vis alle regioner i Europa, sørg for der ikke er nogle dubletter (25 rows) */

SELECT DISTINCT region
FROM country
WHERE Continent = 'Europe';



/* Vis landenavne med et areal under 100 og en forventet levealder på mere end 80 (2 rows) */

SELECT name, surfacearea, lifeexpectancy 
FROM country
WHERE surfacearea < 100 AND lifeexpectancy > 80;

/* Vis alle lande der ikke har nogen HeadOfState (2 rows) */

SELECT name, HeadOfState
FROM Country
WHERE HeadOfState IS NULL OR HeadOfState = '';

/* Vis landekoden for alle lande hvor engelsk er det officielle sprog (44 rows)*/

SELECT CountryCode
FROM CountryLanguage
WHERE [Language] = 'English' AND IsOfficial = 'T';

SELECT CountryCode, [Language], IsOfficial
FROM CountryLanguage
WHERE [Language] = 'English' AND IsOfficial = 'T';

/* Lav en forespørgsel selv inkl. et spørgsmål som ovenstående */

/* 
Vis byer hvor navnets 3. bogstav er et a og det sidste et n. 
Indbyggertal skal være større end 500000. 
Sorter efter landekode Z-A  
*/

SELECT *
FROM city 
WHERE name LIKE '__a%n' AND Population > 500000
ORDER BY CountryCode DESC;