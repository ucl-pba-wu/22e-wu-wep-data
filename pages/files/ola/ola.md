# Spørgsmål 1 -SQL

```SQL
SELECT Name, CountryCode, ID 
FROM City
WHERE CountryCode = 'AUS'
```

Forespørgslen bruger World databasen.  

Hvilke kolonner returnerer forespørgslen ?

1. CountryCode, ID
2. Name, CountryCode, ID
3. CountryCode
4. Name, City, CountryCode

*2 er korrekt*

# Spørgsmål 2 - SQL

```SQL
SELECT *
FROM city
WHERE district IN ('Zuid-Holland') AND name LIKE '%t'
ORDER BY Population ASC;
```
Forespørgslen bruger World databasen.  

Hvad returnerer forespørgslen ?

1. Byer i Zuid-Holland distriktet der har et navn der slutter med t, sorteret stigende efter antal indbyggere
2. Byer i Zuid-Holland distriktet der har et navn der starter med t, sorteret stigende efter bynavn
3. Byer i Zuid-Holland distriktet der har et navn der indeholder t, sorteret faldende efter antal indbyggere
4. Byer i Zuid-Holland distriktet der har et navn der slutter med t, sorteret stigende efter bynavn

*1 er korrekt*

# Spørgsmål 3 - SQL

```SQL
SELECT CountryCode, [Language], IsOfficial
FROM CountryLanguage
WHERE [Language] = 'English' AND IsOfficial = 'F';
```
Forespørgslen bruger World databasen.  

Hvad returnerer forespørgslen ?

1. Incorrect syntax near 'IsOfficial' fordi der er fejl i forespørgslen
2. Lande hvor det officielle sprog er engelsk
3. Lande hvor det officielle sprog ikke er engelsk
4. Lande hvor sproget er engelsk

*3 er korrekt*

# Spørgsmål 4 - SQL

```SQL
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) < 5000000
ORDER BY SumPerRegion DESC;
```
Forespørgslen bruger World databasen.

Hvilke kolonner returnerer forespørgslen ?  

1. country, region, Sum
2. region, population, sum
3. region, SumPerRegion, country, population
4. region, SumPerRegion

*4 er korrekt*

# Spørgsmål 5 - SQL

Følgende Stored Procedure er oprettet i world databasen

```SQL
CREATE PROCEDURE spSumPerRegion
@SumPerRegion INT
AS
BEGIN
SELECT region, SUM(population) AS SumPerRegion
FROM country
GROUP BY region
HAVING SUM(population) > @SumPerRegion
ORDER BY SumPerRegion DESC;
END
```

Hvad er den korrekte syntaks når proceduren skal anvendes i en forespørgsel ?

1. EXEC spSumPerRegion 100000
2. EXEC spSumPerRegion
3. EXECUTE spSumPerRegion
4. Incorrect syntax near 'spSumPerRegion' fordi der er fejl i forespørgslen

*1 er korrekt*

# Spørgsmål 6 - javascript

```js
let number1 = 10
let number2 = 20

function numbers(number3, number4){
    return number3+number4
}

function printStuff(message){
    console.log(message)
}

let answer = numbers(number1, number2)

printStuff(answer)
```

Hvad er korrekt for ovenstående JavaScript kode ?

1. Koden indeholder 3 variabler, 2 funktioner og 3 funktionskald
2. Koden indeholder 2 variabler, 3 funktioner og 2 funktionskald
3. Koden indeholder 3 arrays, 3 funktioner og 2 funktionskald
4. Alle de andre svar muligheder er forkerte


*1 er korrekt*

# Spørgsmål 7 - javascript

```js
const colors = ['black', 'green', 'red', 'yellow']
colors.push('deeppink')

for(let i = 0; i < colors.length; i++){
    console.log(i)
}
```

Hvilke tal udskrives i konsollen, når ovenstående JavaScript program afvikles ?

1. 1 2 3 4 5
2. 0 1 2 3
3. 1 2 3 4
4. 0 1 2 3 4

*4 er korrekt*

# Spørgsmål 8 - javascript

```js
let number1 = 4
let number2 = 2

function numbers(number3, number4){
    return number3+number4
}

console.log(`The magic number is.... ${numbers(number1, number2)}`)
```

Hvad udskrives i konsollen når ovenstående JavaScript program afvikles ?

1. The magic number is.... 42
2. The magic number is.... 6
3. The magic number is.... 7
4. Ingenting, der er fejl i programmet

*2 er korrekt*

# Spørgsmål 9 - javascript

```js
const colors = ['black', 'green', 'red', 'yellow']

console.log(`These colors: ${colors.join(', ')}, are wonderful`)
```

Hvad udskrives i konsollen når ovenstående JavaScript program afvikles ?

1. These colors: blackgreenredyellow are wonderful
2. These colors: black green red yellow are wonderful
3. These colors: deeppink are wonderful
4. These colors: black, green, red, yellow, are wonderful

*4 er korrekt*

# Spørgsmål 10 - javascript

```js
function Amount(number){
    if(number < 50){
        return `Less than half of hundred, Dave`;
    }   
    else if(number > 50){
        return `More than half of hundred, Dave`
    }
    else {
        return `Sorry Dave, I don't understand you`;
    } 
}

console.log(`${Amount(55)}\n${Amount(5)}\n${Amount(50)}`)
```

Hvad udskrives i konsollen når ovenstående JavaScript program afvikles ?

1. More than half of hundred, Dave  
Less than half of hundred, Dave  
Sorry Dave, I don't understand you

2. Amount(55)\nAmount(5)\nAmount(50)

3. Sorry Dave, I don't understand you  
Less than half of hundred, Dave  
More than half of hundred, Dave  

4. 55 5 50

*1 er korrekt*