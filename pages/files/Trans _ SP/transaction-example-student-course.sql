CREATE DATABASE schooldb1;
GO

USE schooldb1;
GO

CREATE TABLE Student
(
    StudentID INT IDENTITY(1,1) PRIMARY KEY,
    Email VARCHAR(100)
)

CREATE TABLE Course
(
    CourseID INT IDENTITY(1,1) PRIMARY KEY,
    Title VARCHAR(200)
)

CREATE TABLE StudentHasCourse
(
    StudentID INT,
    CourseID INT,
    PRIMARY KEY (StudentID, CourseID),
    FOREIGN KEY (StudentID) REFERENCES Student(StudentID),
    FOREIGN KEY (CourseID) REFERENCES Course(CourseID)
)

GO

INSERT INTO Course
VALUES
    ('DB'),
    ('Programming');
GO


-- Author: berb
-- Creation Date: 1/10/2020
-- Insert a student and the course he has signed up for in one transaction
CREATE PROCEDURE spAddStudentAndCourse
    @studentemail VARCHAR(100),
    @courseid INT
AS
BEGIN

    BEGIN TRY
        BEGIN TRANSACTION
            INSERT INTO Student
    VALUES
        (@studentemail);
            INSERT INTO StudentHasCourse
    VALUES
        (SCOPE_IDENTITY(), @courseid);
        COMMIT TRANSACTION
    END TRY

    BEGIN CATCH
		PRINT 'Rolled back:' + ERROR_MESSAGE()
        ROLLBACK
    END CATCH

END
GO

-- Example on rollback, courseid 99 doesn't exist > error > rollback
-- spAddStudentAndCourse 'b777@b1.com', 99
