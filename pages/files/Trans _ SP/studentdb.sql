create database studentdb;
go

use studentdb;
go

create table student (
	studentid int identity(1,1) primary key,
	email varchar(100)
);

create table course (
	courseid int identity(1,1) primary key,
	title varchar(100)
);

create table studenthascourse (
	studentid int,
	courseid int,
	primary key (studentid, courseid),
	foreign key (studentid) references student(studentid),
	foreign key (courseid) references course(courseid)
);


insert into course 
values ('database'), ('programming'), ('ux');