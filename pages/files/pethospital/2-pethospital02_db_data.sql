USE PetHospital02;
GO

INSERT INTO PetOwner(Firstname, Lastname, Phone, Email)
VALUES
           ('Bernie', 'Besenhofer', '99999999', 'berb@ucl.dk'),
		   ('Harry', 'Jensen', '88888888', 'harry@ucl.dk'),
		   ('Dobby', 'Larsen', '77777777', 'dobby@ucl.dk');

INSERT INTO PetType
VALUES ('Bird'), ('Cat'), ('Dog'), ('Fish');

INSERT INTO Pet(PetName, DateBirth, PetOwnerID, PetTypeID)
VALUES 
	('Birdy', '2018-10-24', 1, 1),
	('Mis', '2016-01-01', 1, 2),
	('Fishy', '2019-12-24', 2, 4),
	('Wuff', '2015-08-02', 3, 3),
	('Barky', '2013-02-28', 2, 2);

	
INSERT INTO Treatment(Title, Summary)
VALUES 
	('Eye Wash', null),
	('Rabies Vaccination', null),
	('Annual Checkup', null);

INSERT INTO Visit (VisitDateTime, PetID, InvoiceID)
VALUES 
	('2020-08-01 10:00', 1, null),
	('2020-08-03 12:00', 2, null),
	('2020-08-03 14:00', 3, null),
	('2020-08-03 15:30', 4, null),
	('2020-08-05 09:30', 4, null);

INSERT INTO VisitHasTreatment(VisitID, TreatmentID)
VALUES (1, 1), (1,3), (2, 2), (3,1), (4,2), (4,1), (5,3);

-- Insert data in Invoice and TreatmentPrice tables
-- 

INSERT INTO TreatmentPrice (TreatmentID, PetTypeID, Price)
VALUES (1,1,400), (1,2,300), (1,3, 200);

INSERT INTO TreatmentPrice (TreatmentID, PetTypeID, Price)
VALUES (2,1,40), (2,2,30), (2,3, 20), (2,4, 10);

INSERT INTO Invoice (InvoiceDate, AmountReceived) 
VALUES ('2020-08-31', 1);

UPDATE Visit
SET InvoiceID = 1
WHERE ID = 3

GO